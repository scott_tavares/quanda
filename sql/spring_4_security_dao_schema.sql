-- spring_4_security_dao_schema.sql
-- MySQL Workbench Forward Engineering

SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0;
SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0;
SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='TRADITIONAL,ALLOW_INVALID_DATES';

-- -----------------------------------------------------
-- Schema quanda
-- -----------------------------------------------------
DROP SCHEMA IF EXISTS `quanda` ;

-- -----------------------------------------------------
-- -----------------------------------------------------
-- Schema quanda
-- -----------------------------------------------------

-- -----------------------------------------------------
-- Schema quanda
-- -----------------------------------------------------
CREATE SCHEMA IF NOT EXISTS `quanda` DEFAULT CHARACTER SET utf8 ;
USE `quanda` ;

-- -----------------------------------------------------
-- Table `quanda`.`answer_type_list`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `quanda`.`answer_type_list` ;

CREATE TABLE IF NOT EXISTS `quanda`.`answer_type_list` (
  `idanswer_type_list` INT(11) NOT NULL AUTO_INCREMENT,
  `answer_type_name` VARCHAR(45) NULL DEFAULT NULL,
  PRIMARY KEY (`idanswer_type_list`),
  UNIQUE INDEX `answer_type_name_UNIQUE` (`answer_type_name` ASC))
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8;


-- -----------------------------------------------------
-- Table `quanda`.`quanda`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `quanda`.`quanda` ;

CREATE TABLE IF NOT EXISTS `quanda`.`quanda` (
  `idquanda` INT(11) NOT NULL AUTO_INCREMENT,
  `title` VARCHAR(45) NOT NULL,
  PRIMARY KEY (`idquanda`))
ENGINE = InnoDB
AUTO_INCREMENT = 1002
DEFAULT CHARACTER SET = utf8;


-- -----------------------------------------------------
-- Table `quanda`.`answer_set`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `quanda`.`answer_set` ;

CREATE TABLE IF NOT EXISTS `quanda`.`answer_set` (
  `idanswer_set` INT(11) NOT NULL AUTO_INCREMENT,
  `iidquanda` INT(11) NOT NULL,
  `idanswer_type_list` INT(11) NOT NULL,
  PRIMARY KEY (`idanswer_set`),
  INDEX `fk_answer-set_web-form1_idx` (`iidquanda` ASC),
  INDEX `fk_answer-set_answer-type-list1_idx` (`idanswer_type_list` ASC),
  CONSTRAINT `fk_answer-set_answer-type-list1`
    FOREIGN KEY (`idanswer_type_list`)
    REFERENCES `quanda`.`answer_type_list` (`idanswer_type_list`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_answer-set_web-form1`
    FOREIGN KEY (`iidquanda`)
    REFERENCES `quanda`.`quanda` (`idquanda`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8;


-- -----------------------------------------------------
-- Table `quanda`.`answer`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `quanda`.`answer` ;

CREATE TABLE IF NOT EXISTS `quanda`.`answer` (
  `idanswer` INT(11) NOT NULL AUTO_INCREMENT,
  `idanswer_set` INT(11) NOT NULL,
  PRIMARY KEY (`idanswer`),
  INDEX `fk_answer_answer-set1_idx` (`idanswer_set` ASC),
  CONSTRAINT `fk_answer_answer-set1`
    FOREIGN KEY (`idanswer_set`)
    REFERENCES `quanda`.`answer_set` (`idanswer_set`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8;


-- -----------------------------------------------------
-- Table `quanda`.`answer_dropdown`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `quanda`.`answer_dropdown` ;

CREATE TABLE IF NOT EXISTS `quanda`.`answer_dropdown` (
  `idanswer_dropdown` INT(11) NOT NULL AUTO_INCREMENT,
  PRIMARY KEY (`idanswer_dropdown`),
  CONSTRAINT `fk-answer-dropdown`
    FOREIGN KEY (`idanswer_dropdown`)
    REFERENCES `quanda`.`answer` (`idanswer`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8;


-- -----------------------------------------------------
-- Table `quanda`.`answer_multiple_choice`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `quanda`.`answer_multiple_choice` ;

CREATE TABLE IF NOT EXISTS `quanda`.`answer_multiple_choice` (
  `idanswer_multiple_choice` INT(11) NOT NULL AUTO_INCREMENT,
  PRIMARY KEY (`idanswer_multiple_choice`),
  CONSTRAINT `fk-answer-multiple-choice`
    FOREIGN KEY (`idanswer_multiple_choice`)
    REFERENCES `quanda`.`answer` (`idanswer`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8;


-- -----------------------------------------------------
-- Table `quanda`.`answer_multiple_guess`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `quanda`.`answer_multiple_guess` ;

CREATE TABLE IF NOT EXISTS `quanda`.`answer_multiple_guess` (
  `idanswer_multiple_guess` INT(11) NOT NULL AUTO_INCREMENT,
  PRIMARY KEY (`idanswer_multiple_guess`),
  CONSTRAINT `fk-answer-multiple-guess`
    FOREIGN KEY (`idanswer_multiple_guess`)
    REFERENCES `quanda`.`answer` (`idanswer`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8;


-- -----------------------------------------------------
-- Table `quanda`.`answer_textarea`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `quanda`.`answer_textarea` ;

CREATE TABLE IF NOT EXISTS `quanda`.`answer_textarea` (
  `idanswer_textarea` INT(11) NOT NULL AUTO_INCREMENT,
  PRIMARY KEY (`idanswer_textarea`),
  CONSTRAINT `fk-answer-textarea`
    FOREIGN KEY (`idanswer_textarea`)
    REFERENCES `quanda`.`answer` (`idanswer`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8;

-- -----------------------------------------------------
-- Table `quanda`.`scope`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `quanda`.`scope` ;

CREATE TABLE IF NOT EXISTS `quanda`.`scope` (
  `idscope` INT(11) NOT NULL AUTO_INCREMENT,
  `scope` VARCHAR(45) NULL DEFAULT NULL,
  PRIMARY KEY (`idscope`))
ENGINE = InnoDB
AUTO_INCREMENT = 1004
DEFAULT CHARACTER SET = utf8;


-- -----------------------------------------------------
-- Table `quanda`.`question_group`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `quanda`.`question_group` ;

CREATE TABLE IF NOT EXISTS `quanda`.`question_group` (
  `idquestion-group` INT(11) NOT NULL AUTO_INCREMENT,
  `idquanda` INT(11) NOT NULL,
  `title` VARCHAR(45) NOT NULL,
  `idscope` INT(11) NOT NULL,
  PRIMARY KEY (`idquestion-group`),
  INDEX `fk_question-group_q_and_a1_idx` (`idquanda` ASC),
  INDEX `fk_question-group_scope1_idx` (`idscope` ASC),
  CONSTRAINT `fk_question-group_q_and_a1`
    FOREIGN KEY (`idquanda`)
    REFERENCES `quanda`.`quanda` (`idquanda`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_question-group_scope1`
    FOREIGN KEY (`idscope`)
    REFERENCES `quanda`.`scope` (`idscope`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB
AUTO_INCREMENT = 1002
DEFAULT CHARACTER SET = utf8;


-- -----------------------------------------------------
-- Table `quanda`.`question_type`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `quanda`.`question_type` ;

CREATE TABLE IF NOT EXISTS `quanda`.`question_type` (
  `idquestion_type` INT(11) NOT NULL AUTO_INCREMENT,
  `question_type_name` VARCHAR(45) NULL DEFAULT NULL,
  PRIMARY KEY (`idquestion_type`))
ENGINE = InnoDB
AUTO_INCREMENT = 1005
DEFAULT CHARACTER SET = utf8;


-- -----------------------------------------------------
-- Table `quanda`.`question`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `quanda`.`question` ;

CREATE TABLE IF NOT EXISTS `quanda`.`question` (
  `idquestion` INT(11) NOT NULL AUTO_INCREMENT,
  `question_group_id` INT(11) NOT NULL,
  `idquestion_type` INT(11) NOT NULL,
  `question_text` VARCHAR(4096) NULL DEFAULT NULL,
  PRIMARY KEY (`idquestion`),
  INDEX `fk_question_question-group1_idx` (`question_group_id` ASC),
  INDEX `fk_question_question_type1_idx` (`idquestion_type` ASC),
  CONSTRAINT `fk_question_question-group1`
    FOREIGN KEY (`question_group_id`)
    REFERENCES `quanda`.`question_group` (`idquestion-group`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_question_question_type1`
    FOREIGN KEY (`idquestion_type`)
    REFERENCES `quanda`.`question_type` (`idquestion_type`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB
AUTO_INCREMENT = 1002
DEFAULT CHARACTER SET = utf8;


-- -----------------------------------------------------
-- Table `quanda`.`question_group_has_question_group`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `quanda`.`question_group_has_question_group` ;

CREATE TABLE IF NOT EXISTS `quanda`.`question_group_has_question_group` (
  `idquestion_group` INT(11) NOT NULL,
  `idquestion_group_sub` INT(11) NOT NULL,
  PRIMARY KEY (`idquestion_group`, `idquestion_group_sub`),
  INDEX `fk_question-group_has_question-group_question-group1_idx` (`idquestion_group_sub` ASC),
  INDEX `fk_question-group_has_question-group_question-group_idx` (`idquestion_group` ASC),
  CONSTRAINT `fk_question-group_has_question-group_question-group`
    FOREIGN KEY (`idquestion_group`)
    REFERENCES `quanda`.`question_group` (`idquestion-group`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_question-group_has_question-group_question-group1`
    FOREIGN KEY (`idquestion_group_sub`)
    REFERENCES `quanda`.`question_group` (`idquestion-group`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8;


-- -----------------------------------------------------
-- Table `quanda`.`question_item`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `quanda`.`question_item` ;

CREATE TABLE IF NOT EXISTS `quanda`.`question_item` (
  `idquestion_item` INT(11) NOT NULL AUTO_INCREMENT,
  `question_idquestion` INT(11) NOT NULL,
  `question_item_text` VARCHAR(255) NULL DEFAULT NULL,
  PRIMARY KEY (`idquestion_item`),
  INDEX `fk_question_item_question1_idx` (`question_idquestion` ASC),
  CONSTRAINT `fk_question_item_question1`
    FOREIGN KEY (`question_idquestion`)
    REFERENCES `quanda`.`question` (`idquestion`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8;

-- -----------------------------------------------------
-- -----------------------------------------------------
-- -----------------------------------------------------
-- -----------------------------------------------------
-- -----------------------------------------------------
-- -----------------------------------------------------
DROP TABLE IF EXISTS `acl_sid`;
CREATE TABLE acl_sid (
	id BIGINT UNSIGNED NOT NULL AUTO_INCREMENT PRIMARY KEY,
	principal BOOLEAN NOT NULL,
	sid VARCHAR(100) NOT NULL,
	UNIQUE KEY unique_acl_sid (sid, principal)
) ENGINE=InnoDB;

DROP TABLE IF EXISTS `acl_class`;
CREATE TABLE acl_class (
	id BIGINT UNSIGNED NOT NULL AUTO_INCREMENT PRIMARY KEY,
	class VARCHAR(100) NOT NULL,
	UNIQUE KEY uk_acl_class (class)
) ENGINE=InnoDB;

DROP TABLE IF EXISTS `acl_object_identity`;
CREATE TABLE acl_object_identity (
	id BIGINT UNSIGNED NOT NULL AUTO_INCREMENT PRIMARY KEY,
	object_id_class BIGINT UNSIGNED NOT NULL,
	object_id_identity BIGINT NOT NULL,
	parent_object BIGINT UNSIGNED,
	owner_sid BIGINT UNSIGNED,
	entries_inheriting BOOLEAN NOT NULL,
	UNIQUE KEY uk_acl_object_identity (object_id_class, object_id_identity),
	CONSTRAINT fk_acl_object_identity_parent FOREIGN KEY (parent_object) REFERENCES acl_object_identity (id),
	CONSTRAINT fk_acl_object_identity_class FOREIGN KEY (object_id_class) REFERENCES acl_class (id),
	CONSTRAINT fk_acl_object_identity_owner FOREIGN KEY (owner_sid) REFERENCES acl_sid (id)
) ENGINE=InnoDB;

DROP TABLE IF EXISTS `acl_entry`;
CREATE TABLE acl_entry (
	id BIGINT UNSIGNED NOT NULL AUTO_INCREMENT PRIMARY KEY,
	acl_object_identity BIGINT UNSIGNED NOT NULL,
	ace_order INTEGER NOT NULL,
	sid BIGINT UNSIGNED NOT NULL,
	mask INTEGER UNSIGNED NOT NULL,
	granting BOOLEAN NOT NULL,
	audit_success BOOLEAN NOT NULL,
	audit_failure BOOLEAN NOT NULL,
	UNIQUE KEY unique_acl_entry (acl_object_identity, ace_order),
	CONSTRAINT fk_acl_entry_object FOREIGN KEY (acl_object_identity) REFERENCES acl_object_identity (id),
	CONSTRAINT fk_acl_entry_acl FOREIGN KEY (sid) REFERENCES acl_sid (id)
) ENGINE=InnoDB;

DROP TABLE IF EXISTS `fixed_deposit_details`;
CREATE TABLE IF NOT EXISTS `fixed_deposit_details` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `user` varchar(50) NOT NULL,
  `amount` varchar(50) NOT NULL,
  `tenure` varchar(50) NOT NULL,
  `email` varchar(50) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COMMENT='Contains fixed deposit details';

DROP TABLE IF EXISTS `users`;
CREATE TABLE IF NOT EXISTS `users` (
  `username` varchar(50) NOT NULL,
  `password` varchar(50) NOT NULL,
  `enabled` tinyint(1) NOT NULL,
  PRIMARY KEY (`username`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

DROP TABLE IF EXISTS `authorities`;
CREATE TABLE IF NOT EXISTS `authorities` (
  `username` varchar(50) NOT NULL,
  `authority` varchar(50) NOT NULL,
  PRIMARY KEY (`username`,`authority`),
  UNIQUE KEY `authorities_idx_1` (`username`,`authority`),
  CONSTRAINT `authorities_ibfk_1` FOREIGN KEY (`username`) REFERENCES `users` (`username`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

SET SQL_MODE=@OLD_SQL_MODE;
SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS;
SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS;

-- -----------------------------------------------------
-- Data for table `quanda`.`user`
-- -----------------------------------------------------
START TRANSACTION;
USE `quanda`;

INSERT INTO `acl_class` (`id`, `class`) VALUES
	(1, 'sample.spring.chapter16.domain.FixedDepositDetails');

INSERT INTO `acl_sid` (`id`, `principal`, `sid`) VALUES
	(3, 1, 'admin'),
	(1, 1, 'cust1'),
	(2, 1, 'cust2');

INSERT INTO `users` (`username`, `password`, `enabled`) VALUES
	('admin', 'admin', 1),
	('cust1', 'cust1', 1),
	('cust2', 'cust2', 1);
	
INSERT INTO `authorities` (`username`, `authority`) VALUES
	('admin', 'ROLE_ADMIN'),
	('cust1', 'ROLE_CUSTOMER'),
	('cust2', 'ROLE_CUSTOMER');

COMMIT;

