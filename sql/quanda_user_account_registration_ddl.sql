-- quanda_user_account_registration_ddl.sql

-- MySQL Workbench Forward Engineering
SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0;
SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0;
SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='TRADITIONAL,ALLOW_INVALID_DATES';

-- -----------------------------------------------------
-- Schema quanda
-- -----------------------------------------------------
DROP SCHEMA IF EXISTS `quanda` ;

-- -----------------------------------------------------

-- -----------------------------------------------------
-- Schema quanda
-- -----------------------------------------------------
CREATE SCHEMA IF NOT EXISTS `quanda` DEFAULT CHARACTER SET utf8 ;
USE `quanda` ;

-- -----------------------------------------------------
-- Table `quanda`.`answer_type_list`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `quanda`.`answer_type_list` ;

CREATE TABLE IF NOT EXISTS `quanda`.`answer_type_list` (
  `idanswer_type_list` INT(11) NOT NULL AUTO_INCREMENT,
  `answer_type_name` VARCHAR(45) NULL DEFAULT NULL,
  PRIMARY KEY (`idanswer_type_list`),
  UNIQUE INDEX `answer_type_name_UNIQUE` (`answer_type_name` ASC))
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8;


-- -----------------------------------------------------
-- Table `quanda`.`quanda`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `quanda`.`quanda` ;

CREATE TABLE IF NOT EXISTS `quanda`.`quanda` (
  `idquanda` INT(11) NOT NULL AUTO_INCREMENT,
  `title` VARCHAR(45) NOT NULL,
  PRIMARY KEY (`idquanda`))
ENGINE = InnoDB
AUTO_INCREMENT = 1002
DEFAULT CHARACTER SET = utf8;


-- -----------------------------------------------------
-- Table `quanda`.`answer_set`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `quanda`.`answer_set` ;

CREATE TABLE IF NOT EXISTS `quanda`.`answer_set` (
  `idanswer_set` INT(11) NOT NULL AUTO_INCREMENT,
  `iidquanda` INT(11) NOT NULL,
  `idanswer_type_list` INT(11) NOT NULL,
  PRIMARY KEY (`idanswer_set`),
  INDEX `fk_answer-set_web-form1_idx` (`iidquanda` ASC),
  INDEX `fk_answer-set_answer-type-list1_idx` (`idanswer_type_list` ASC),
  CONSTRAINT `fk_answer-set_answer-type-list1`
    FOREIGN KEY (`idanswer_type_list`)
    REFERENCES `quanda`.`answer_type_list` (`idanswer_type_list`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_answer-set_web-form1`
    FOREIGN KEY (`iidquanda`)
    REFERENCES `quanda`.`quanda` (`idquanda`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8;


-- -----------------------------------------------------
-- Table `quanda`.`answer`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `quanda`.`answer` ;

CREATE TABLE IF NOT EXISTS `quanda`.`answer` (
  `idanswer` INT(11) NOT NULL AUTO_INCREMENT,
  `idanswer_set` INT(11) NOT NULL,
  PRIMARY KEY (`idanswer`),
  INDEX `fk_answer_answer-set1_idx` (`idanswer_set` ASC),
  CONSTRAINT `fk_answer_answer-set1`
    FOREIGN KEY (`idanswer_set`)
    REFERENCES `quanda`.`answer_set` (`idanswer_set`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8;


-- -----------------------------------------------------
-- Table `quanda`.`answer_dropdown`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `quanda`.`answer_dropdown` ;

CREATE TABLE IF NOT EXISTS `quanda`.`answer_dropdown` (
  `idanswer_dropdown` INT(11) NOT NULL AUTO_INCREMENT,
  PRIMARY KEY (`idanswer_dropdown`),
  CONSTRAINT `fk-answer-dropdown`
    FOREIGN KEY (`idanswer_dropdown`)
    REFERENCES `quanda`.`answer` (`idanswer`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8;


-- -----------------------------------------------------
-- Table `quanda`.`answer_multiple_choice`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `quanda`.`answer_multiple_choice` ;

CREATE TABLE IF NOT EXISTS `quanda`.`answer_multiple_choice` (
  `idanswer_multiple_choice` INT(11) NOT NULL AUTO_INCREMENT,
  PRIMARY KEY (`idanswer_multiple_choice`),
  CONSTRAINT `fk-answer-multiple-choice`
    FOREIGN KEY (`idanswer_multiple_choice`)
    REFERENCES `quanda`.`answer` (`idanswer`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8;


-- -----------------------------------------------------
-- Table `quanda`.`answer_multiple_guess`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `quanda`.`answer_multiple_guess` ;

CREATE TABLE IF NOT EXISTS `quanda`.`answer_multiple_guess` (
  `idanswer_multiple_guess` INT(11) NOT NULL AUTO_INCREMENT,
  PRIMARY KEY (`idanswer_multiple_guess`),
  CONSTRAINT `fk-answer-multiple-guess`
    FOREIGN KEY (`idanswer_multiple_guess`)
    REFERENCES `quanda`.`answer` (`idanswer`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8;


-- -----------------------------------------------------
-- Table `quanda`.`answer_textarea`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `quanda`.`answer_textarea` ;

CREATE TABLE IF NOT EXISTS `quanda`.`answer_textarea` (
  `idanswer_textarea` INT(11) NOT NULL AUTO_INCREMENT,
  PRIMARY KEY (`idanswer_textarea`),
  CONSTRAINT `fk-answer-textarea`
    FOREIGN KEY (`idanswer_textarea`)
    REFERENCES `quanda`.`answer` (`idanswer`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8;

-- -----------------------------------------------------
-- Table `quanda`.`scope`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `quanda`.`scope` ;

CREATE TABLE IF NOT EXISTS `quanda`.`scope` (
  `idscope` INT(11) NOT NULL AUTO_INCREMENT,
  `scope` VARCHAR(45) NULL DEFAULT NULL,
  PRIMARY KEY (`idscope`))
ENGINE = InnoDB
AUTO_INCREMENT = 1004
DEFAULT CHARACTER SET = utf8;


-- -----------------------------------------------------
-- Table `quanda`.`question_group`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `quanda`.`question_group` ;

CREATE TABLE IF NOT EXISTS `quanda`.`question_group` (
  `idquestion-group` INT(11) NOT NULL AUTO_INCREMENT,
  `idquanda` INT(11) NOT NULL,
  `title` VARCHAR(45) NOT NULL,
  `idscope` INT(11) NOT NULL,
  PRIMARY KEY (`idquestion-group`),
  INDEX `fk_question-group_q_and_a1_idx` (`idquanda` ASC),
  INDEX `fk_question-group_scope1_idx` (`idscope` ASC),
  CONSTRAINT `fk_question-group_q_and_a1`
    FOREIGN KEY (`idquanda`)
    REFERENCES `quanda`.`quanda` (`idquanda`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_question-group_scope1`
    FOREIGN KEY (`idscope`)
    REFERENCES `quanda`.`scope` (`idscope`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB
AUTO_INCREMENT = 1002
DEFAULT CHARACTER SET = utf8;


-- -----------------------------------------------------
-- Table `quanda`.`question_type`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `quanda`.`question_type` ;

CREATE TABLE IF NOT EXISTS `quanda`.`question_type` (
  `idquestion_type` INT(11) NOT NULL AUTO_INCREMENT,
  `question_type_name` VARCHAR(45) NULL DEFAULT NULL,
  PRIMARY KEY (`idquestion_type`))
ENGINE = InnoDB
AUTO_INCREMENT = 1005
DEFAULT CHARACTER SET = utf8;


-- -----------------------------------------------------
-- Table `quanda`.`question`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `quanda`.`question` ;

CREATE TABLE IF NOT EXISTS `quanda`.`question` (
  `idquestion` INT(11) NOT NULL AUTO_INCREMENT,
  `question_group_id` INT(11) NOT NULL,
  `idquestion_type` INT(11) NOT NULL,
  `question_text` VARCHAR(4096) NULL DEFAULT NULL,
  PRIMARY KEY (`idquestion`),
  INDEX `fk_question_question-group1_idx` (`question_group_id` ASC),
  INDEX `fk_question_question_type1_idx` (`idquestion_type` ASC),
  CONSTRAINT `fk_question_question-group1`
    FOREIGN KEY (`question_group_id`)
    REFERENCES `quanda`.`question_group` (`idquestion-group`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_question_question_type1`
    FOREIGN KEY (`idquestion_type`)
    REFERENCES `quanda`.`question_type` (`idquestion_type`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB
AUTO_INCREMENT = 1002
DEFAULT CHARACTER SET = utf8;


-- -----------------------------------------------------
-- Table `quanda`.`question_group_has_question_group`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `quanda`.`question_group_has_question_group` ;

CREATE TABLE IF NOT EXISTS `quanda`.`question_group_has_question_group` (
  `idquestion_group` INT(11) NOT NULL,
  `idquestion_group_sub` INT(11) NOT NULL,
  PRIMARY KEY (`idquestion_group`, `idquestion_group_sub`),
  INDEX `fk_question-group_has_question-group_question-group1_idx` (`idquestion_group_sub` ASC),
  INDEX `fk_question-group_has_question-group_question-group_idx` (`idquestion_group` ASC),
  CONSTRAINT `fk_question-group_has_question-group_question-group`
    FOREIGN KEY (`idquestion_group`)
    REFERENCES `quanda`.`question_group` (`idquestion-group`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_question-group_has_question-group_question-group1`
    FOREIGN KEY (`idquestion_group_sub`)
    REFERENCES `quanda`.`question_group` (`idquestion-group`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8;


-- -----------------------------------------------------
-- Table `quanda`.`question_item`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `quanda`.`question_item` ;

CREATE TABLE IF NOT EXISTS `quanda`.`question_item` (
  `idquestion_item` INT(11) NOT NULL AUTO_INCREMENT,
  `question_idquestion` INT(11) NOT NULL,
  `question_item_text` VARCHAR(255) NULL DEFAULT NULL,
  PRIMARY KEY (`idquestion_item`),
  INDEX `fk_question_item_question1_idx` (`question_idquestion` ASC),
  CONSTRAINT `fk_question_item_question1`
    FOREIGN KEY (`question_idquestion`)
    REFERENCES `quanda`.`question` (`idquestion`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8;

-- -------------------------------------------------------------
-- -------------------------------------------------------------
-- -------------------------------------------------------------
-- -------------------------------------------------------------
-- -------------------------------------------------------------
-- -------------------------------------------------------------
-- -----------------------------------------------------
-- Table `quanda`.`user_account`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `quanda`.`user_account` ;

CREATE TABLE IF NOT EXISTS `quanda`.`user_account` (
  `ID` BIGINT(20) NOT NULL AUTO_INCREMENT,
  `EMAIL` VARCHAR(255) NULL DEFAULT NULL,
  `ENABLED` TINYINT(1) NULL DEFAULT '0',
  `FIRSTNAME` VARCHAR(255) NULL DEFAULT NULL,
  `ISUSING2FA` TINYINT(1) NULL DEFAULT '0',
  `LASTNAME` VARCHAR(255) NULL DEFAULT NULL,
  `PASSWORD` VARCHAR(60) NULL DEFAULT NULL,
  `SECRET` VARCHAR(255) NULL DEFAULT NULL,
  PRIMARY KEY (`ID`))
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8;


-- -----------------------------------------------------
-- Table `quanda`.`passwordresettoken`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `quanda`.`passwordresettoken` ;

CREATE TABLE IF NOT EXISTS `quanda`.`passwordresettoken` (
  `ID` BIGINT(20) NOT NULL AUTO_INCREMENT,
  `EXPIRYDATE` DATE NULL DEFAULT NULL,
  `TOKEN` VARCHAR(255) NULL DEFAULT NULL,
  `user_id` BIGINT(20) NOT NULL,
  PRIMARY KEY (`ID`),
  INDEX `FK_PASSWORDRESETTOKEN_user_id` (`user_id` ASC),
  CONSTRAINT `FK_PASSWORDRESETTOKEN_user_id`
    FOREIGN KEY (`user_id`)
    REFERENCES `quanda`.`user_account` (`ID`))
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8;


-- -----------------------------------------------------
-- Table `quanda`.`privilege`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `quanda`.`privilege` ;

CREATE TABLE IF NOT EXISTS `quanda`.`privilege` (
  `ID` BIGINT(20) NOT NULL AUTO_INCREMENT,
  `NAME` VARCHAR(255) NULL DEFAULT NULL,
  PRIMARY KEY (`ID`))
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8;


-- -----------------------------------------------------
-- Table `quanda`.`role`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `quanda`.`role` ;

CREATE TABLE IF NOT EXISTS `quanda`.`role` (
  `ID` BIGINT(20) NOT NULL AUTO_INCREMENT,
  `NAME` VARCHAR(255) NULL DEFAULT NULL,
  PRIMARY KEY (`ID`))
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8;


-- -----------------------------------------------------
-- Table `quanda`.`roles_privileges`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `quanda`.`roles_privileges` ;

CREATE TABLE IF NOT EXISTS `quanda`.`roles_privileges` (
  `role_id` BIGINT(20) NOT NULL,
  `privilege_id` BIGINT(20) NOT NULL,
  PRIMARY KEY (`role_id`, `privilege_id`),
  INDEX `FK_roles_privileges_privilege_id` (`privilege_id` ASC),
  CONSTRAINT `FK_roles_privileges_privilege_id`
    FOREIGN KEY (`privilege_id`)
    REFERENCES `quanda`.`privilege` (`ID`),
  CONSTRAINT `FK_roles_privileges_role_id`
    FOREIGN KEY (`role_id`)
    REFERENCES `quanda`.`role` (`ID`))
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8;


-- -----------------------------------------------------
-- Table `quanda`.`users_roles`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `quanda`.`users_roles` ;

CREATE TABLE IF NOT EXISTS `quanda`.`users_roles` (
  `role_id` BIGINT(20) NOT NULL,
  `user_id` BIGINT(20) NOT NULL,
  PRIMARY KEY (`role_id`, `user_id`),
  INDEX `FK_users_roles_user_id` (`user_id` ASC),
  CONSTRAINT `FK_users_roles_role_id`
    FOREIGN KEY (`role_id`)
    REFERENCES `quanda`.`role` (`ID`),
  CONSTRAINT `FK_users_roles_user_id`
    FOREIGN KEY (`user_id`)
    REFERENCES `quanda`.`user_account` (`ID`))
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8;


-- -----------------------------------------------------
-- Table `quanda`.`verificationtoken`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `quanda`.`verificationtoken` ;

CREATE TABLE IF NOT EXISTS `quanda`.`verificationtoken` (
  `ID` BIGINT(20) NOT NULL AUTO_INCREMENT,
  `EXPIRYDATE` DATE NULL DEFAULT NULL,
  `TOKEN` VARCHAR(255) NULL DEFAULT NULL,
  `user_id` BIGINT(20) NOT NULL,
  PRIMARY KEY (`ID`))
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8;

SET SQL_MODE=@OLD_SQL_MODE;
SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS;
SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS;
