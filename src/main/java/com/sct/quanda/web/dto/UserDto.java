package com.sct.quanda.web.dto;

import com.sct.quanda.web.annotation.NotEmpty;
import com.sct.quanda.web.annotation.PasswordMatcher;
import com.sct.quanda.web.annotation.ValidEmail;
import java.io.Serializable;
import javax.validation.constraints.NotNull;

/**
 *
 * @author stavares
 */
@PasswordMatcher
public class UserDto implements Serializable {

  @NotNull
  @NotEmpty
  private String firstName;

  @NotNull
  @NotEmpty
  private String lastName;

  @NotNull
  @NotEmpty
  private String password;

  @NotNull
  @NotEmpty
  private String passwordMatcher;

  @NotNull
  @NotEmpty
  @ValidEmail
  private String email;

  /**
   * @return the firstName
   */
  public String getFirstName() {
    return firstName;
  }

  /**
   * @param firstName the firstName to set
   */
  public void setFirstName(String firstName) {
    this.firstName = firstName;
  }

  /**
   * @return the lastName
   */
  public String getLastName() {
    return lastName;
  }

  /**
   * @param lastName the lastName to set
   */
  public void setLastName(String lastName) {
    this.lastName = lastName;
  }

  /**
   * @return the password
   */
  public String getPassword() {
    return password;
  }

  /**
   * @param password the password to set
   */
  public void setPassword(String password) {
    this.password = password;
  }

  /**
   * @return the passwordMatcher
   */
  public String getPasswordMatcher() {
    return passwordMatcher;
  }

  /**
   * @param passwordMatcher the passwordMatcher to set
   */
  public void setPasswordMatcher(String passwordMatcher) {
    this.passwordMatcher = passwordMatcher;
  }

  /**
   * @return the email
   */
  public String getEmail() {
    return email;
  }

  /**
   * @param email the email to set
   */
  public void setEmail(String email) {
    this.email = email;
  }

}
