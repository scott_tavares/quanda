package com.sct.quanda.web.annotation;

import com.sct.quanda.web.dto.UserDto;
import java.util.Locale;
import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;

/**
 *
 * @author stavares
 */
class PasswordMatcherValidator implements ConstraintValidator<PasswordMatcher, Object> {

  @Autowired
  MessageSource MessageSource;

  @Override
  public void initialize(PasswordMatcher constraintAnnotation) {
  }

  @Override
  public boolean isValid(Object value, ConstraintValidatorContext context) {
    String message = MessageSource.getMessage("password.matcher.validator",
            null, Locale.getDefault());
    
    UserDto user = (UserDto) value;
    if(user.getPassword().equals(user.getPasswordMatcher())) return true;
    context.buildConstraintViolationWithTemplate(message)
            .addPropertyNode("passwordMatcher").addConstraintViolation();
    return false;
  }
}
