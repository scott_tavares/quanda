package com.sct.quanda.web.annotation;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;

/**
 *
 * @author stavares
 */
class NotEmptyValidator implements ConstraintValidator<NotEmpty, String> {

  @Override
  public void initialize(NotEmpty constraintAnnotation) {
  }

  @Override
  public boolean isValid(String value, ConstraintValidatorContext context) {
    return value != null ? !value.trim().isEmpty() : false;
  }
}
