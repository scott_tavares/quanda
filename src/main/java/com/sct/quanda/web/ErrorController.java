package com.sct.quanda.web;

import java.util.HashMap;
import java.util.Locale;
import java.util.Map;
import javax.servlet.http.HttpServletRequest;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

/**
 *
 * @author stavares
 */
@Controller
@RequestMapping(path = "/error")
public class ErrorController {

  private static final Logger LOG = Logger.getLogger(ErrorController.class.getName());

  @Autowired
  private MessageSource messageSource;

  @RequestMapping(value = {"/invalidToken"}, method = RequestMethod.GET)
  public ModelAndView invalidToken(
          HttpServletRequest request){

    Map<String, Object> modelData = new HashMap<>();
    ModelAndView model = new ModelAndView();
    Locale locale = request.getLocale();
    String message = messageSource.getMessage("auth.message.invalidToken", null, locale);
    modelData.put("message", message);
    return new ModelAndView("badUserPage", modelData);
  }

  @RequestMapping(value = {"/expiredToken"}, method = RequestMethod.GET)
  public ModelAndView expiredToken(
          HttpServletRequest request,
          @RequestParam("token") String token){

    LOG.info("In errorController::expiredToken(): token = " + token);

    Map<String, Object> modelData = new HashMap<>();
    ModelAndView model = new ModelAndView();
    Locale locale = request.getLocale();
    String message = messageSource.getMessage("auth.message.expired", null, locale);
    modelData.put("message", message);
    modelData.put("expired", true);
    modelData.put("token", token);

    return new ModelAndView("badUserPage", modelData);
  }
}
