package com.sct.quanda.web;

import org.apache.log4j.Logger;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

/**
 *
 * @author stavares
 */
@Controller
public class LoginController {

  private static final Logger LOG =
          Logger.getLogger(LoginController.class.getName());

  @RequestMapping(value = "/login", method = {RequestMethod.GET, RequestMethod.POST})
  public ModelAndView loginPage() {
    ModelAndView model = new ModelAndView();
    model.setViewName("loginPage");
    return model;
  }

  @RequestMapping(value = "/accessdenied", method = {RequestMethod.POST ,RequestMethod.GET})
  public String accessDenied(){
    return "accessDeniedPage";
  }
}
