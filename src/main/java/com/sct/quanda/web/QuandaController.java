package com.sct.quanda.web;

import com.sct.quanda.model.Quanda;
import com.sct.quanda.service.QuandaService;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.logging.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.SessionAttributes;
import org.springframework.web.servlet.ModelAndView;

/**
 *
 * @author stavares
 */
@Controller
@RequestMapping(path = "/top")
@SessionAttributes(names = {"quandaList","sessionMessage"})
public class QuandaController {

  private static final Logger LOG
          = Logger.getLogger(QuandaController.class.getName());

  @Autowired
  private QuandaService quandaService;

  @RequestMapping(path = "/page", method = RequestMethod.GET)
  public ModelAndView sayHello(@ModelAttribute("quandaList") List<Quanda> quandaList) {

    Map<String, Object> modelData = new HashMap<>();
    modelData.put("msg", "Hello World From Quanda Controller!! QList has " + quandaList.size());
    modelData.put("quandaList", quandaList);

    return new ModelAndView("topPage", modelData);
  }

  @ModelAttribute("quandaList")
  List<Quanda> getQuandaList() {
    LOG.info("Adding quandaList to the model");
    return quandaService.listQuanda();
  }

  @ModelAttribute("sessionMessage")
  public String getSessionMessage(){
    return "This is the origional Message!";
  }
}
