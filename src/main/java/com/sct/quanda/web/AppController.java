package com.sct.quanda.web;

import com.sct.quanda.model.UserAccount;
import com.sct.quanda.service.UserAccountService;
import java.util.Locale;
import java.util.logging.Logger;
import javax.servlet.http.HttpServletRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.AnonymousAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.web.csrf.DefaultCsrfToken;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

/**
 *
 * @author stavares
 */
@Controller
public class AppController {

  private static final Logger LOG
          = Logger.getLogger(AppController.class.getName());

  @Autowired
  UserAccountService userAccountService;

  @RequestMapping(value = {"/"}, method = RequestMethod.GET)
  public ModelAndView welcomePage(Locale locale, HttpServletRequest request) {
    ModelAndView model = new ModelAndView();
    model.setViewName("welcomePage");
    return model;
  }

  @RequestMapping(value = {"/home"}, method = RequestMethod.GET)
  public ModelAndView homePage(HttpServletRequest request) throws Exception {
    ModelAndView model = new ModelAndView();
    DefaultCsrfToken _csrf = (DefaultCsrfToken) request.getAttribute("_csrf");
    request.getSession().setAttribute("_csrf", _csrf);
    UserAccount user = userAccountService.findByEmail(getUserName());
    model.addObject("user", user);
    model.setViewName("homePage");
    return model;
  }

  private String getUserName() throws Exception {
    Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
    if (!(authentication instanceof AnonymousAuthenticationToken)) {
      String currentUserName = authentication.getName();
      return currentUserName;
    }
    throw new Exception("no user found!");
  }
}
