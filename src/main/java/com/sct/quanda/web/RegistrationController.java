package com.sct.quanda.web;

import com.sct.quanda.model.UserAccount;
import com.sct.quanda.model.Verificationtoken;
import com.sct.quanda.registration.OnRegistrationCompleteEvent;
import com.sct.quanda.service.EmailExistsException;
import com.sct.quanda.service.UserAccountService;
import com.sct.quanda.web.dto.UserDto;
import com.sct.quanda.web.util.GenericResponse;
import java.util.Calendar;
import java.util.Locale;
import java.util.UUID;
import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationEventPublisher;
import org.springframework.context.MessageSource;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.SessionAttributes;
import org.springframework.web.bind.support.SessionStatus;
import org.springframework.web.servlet.ModelAndView;

/**
 *
 * @author stavares
 */
@Controller
@RequestMapping(path = "/reg")
@SessionAttributes(names = {"newUser", "editUser", "message"})
public class RegistrationController {

  @Autowired
  UserAccountService userAccountService;

  @Autowired
  ApplicationEventPublisher eventPublisher;

  @Autowired
  private MessageSource messageSource;

  private static final Logger LOG =
          Logger.getLogger(RegistrationController.class.getName());

  @RequestMapping(value = {"/new"}, method = RequestMethod.GET)
  public ModelAndView newRegistration(HttpServletRequest request) throws Exception {
    ModelAndView model = new ModelAndView();
    model.setViewName("registationPage");
    return model;
  }

  @ModelAttribute("newUser")
  public UserDto getNewUser() {
    UserDto userDto = new UserDto();
    return userDto;
  }

  /**
   * Registration Confirmation
   *
   * @param request
   * @param model
   * @param token
   * @param sessionStatus
   * @return
   */
  @RequestMapping(value = {"/registrationConfirmation"}, method = RequestMethod.GET)
  public String emailConfirm(
          HttpServletRequest request,
          Model model,
          @RequestParam("token") String token,
          SessionStatus sessionStatus) {

    request.getSession().invalidate();
    Locale locale = request.getLocale();
    Verificationtoken verificationtoken
            = userAccountService.getVerificationToken(token);
    if (verificationtoken == null) {
      return "redirect:/error/invalidToken?token=" + token + "&lang="
              + locale.getLanguage();
    }

    UserAccount userAccount = verificationtoken.getUserAccount();
    Calendar cal = Calendar.getInstance();
    if (((verificationtoken.getExpirydate().getTime())
            - (cal.getTime().getTime())) <= 0) {
      LOG.info("Calling verificationtoken.getExpirydate");
      return "redirect:/error/expiredToken?token=" + token + "&lang="
              + locale.getLanguage();
    }

    // Enable the UserAccount
    userAccount.setEnabled(true);
    userAccountService.saveUserAccount(userAccount);
    sessionStatus.isComplete();
    return "redirect:/login?lang=" + locale.getLanguage();
  }

  /**
   * CREATE new user account
   * @param request
   * @param userDto
   * @param bindingResult
   * @param sessionStatus
   * @return
   */
  @RequestMapping(params = "fdAction=create", method = RequestMethod.POST)
  public ModelAndView createNewRegistration(
          HttpServletRequest request,
          @ModelAttribute(name = "newUser") @Valid UserDto userDto,
          BindingResult bindingResult,
          SessionStatus sessionStatus) {

    LOG.debug("called RegistrationController::createNewRegistration()");
    ModelAndView model = new ModelAndView();

    if (bindingResult.hasErrors()) { // Check binding errors
      model.setViewName("registationPage");
    } else { // Attempt to create the account

      UserAccount userAccount = new UserAccount();
      userAccount = createUserAccount(userDto, bindingResult);

      if (userAccount == null) { // EMail address alrerady exists

        LOG.info("ERROR Creating UserAccount");
        bindingResult.rejectValue("email", "message.regError");
        model.setViewName("registationPage");

      } else { // Create the account

        String token = UUID.randomUUID().toString();
        userAccountService.createVerificationToken(userAccount, token);

        ////////////////////////////////////////////////////////////////////////
        // Send EMail verificationUrl
        // TODO: Make this call async
        String verificationUrl
                = "http://" + request.getServerName() + ":"
                + request.getServerPort() + request.getContextPath()
                + "/reg/registrationConfirmation";

        eventPublisher.publishEvent(new OnRegistrationCompleteEvent(
                userAccount,
                userAccountService.getVerificationToken(token),
                verificationUrl,
                request.getLocale()));

        ////////////////////////////////////////////////////////////////////////
        // Finsh up
        sessionStatus.setComplete();
        model.addObject("registration", "Registration Successeeded");
        model.setViewName("loginPage");
      }
    }
    return model;
  }

  /**
   * RESEND Registration Token
   *
   * @param request
   * @param existingToken
   * @param sessionStatus
   * @return
   */
  @ResponseBody
  @RequestMapping(value = "/resendRegistrationToken", method = RequestMethod.GET)
  public GenericResponse resendRegistrationToken(
          HttpServletRequest request,
          @RequestParam("token") String existingToken,
          SessionStatus sessionStatus) {

    LOG.debug("In RegistrationController::resendRegistrationToken(): token = "
            + existingToken);

    Verificationtoken newToken
            = userAccountService.generateNewVerificationToken(existingToken);
    UserAccount userAccount = newToken.getUserAccount();

    // TODO: Make this call async
    String verificationUrl
                = "http://" + request.getServerName() + ":"
                + request.getServerPort() + request.getContextPath()
                + "/reg/registrationConfirmation";

    eventPublisher.publishEvent(new OnRegistrationCompleteEvent(
            userAccount,
            newToken,
            verificationUrl,
            request.getLocale()));

    sessionStatus.setComplete();
    return new GenericResponse(
            messageSource.getMessage("message.resendToken", null, request.getLocale()));
  }

  private UserAccount createUserAccount(UserDto accountDto, BindingResult result) {

    LOG.debug("called RegistrationController::createNewRegistration()::"
            + "createUserAccount()");
    UserAccount userAccount = null;
    try {
      userAccount = userAccountService.registerNewUserAccount(accountDto);
      LOG.debug("**** Return from RegistrationController::createNewRegistration()::"
              + "createUserAccount()::"
              + "userAccountService.registerNewUserAccount(accountDto)");
    } catch (EmailExistsException e) {
      return null;
    }
    LOG.debug("Return from RegistrationController::createNewRegistration()::"
            + "createUserAccount()");
    return userAccount;
  }
}
