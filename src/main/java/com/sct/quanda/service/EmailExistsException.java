package com.sct.quanda.service;

/**
 *
 * @author stavares
 */
public class EmailExistsException extends Exception {

  public EmailExistsException(String message) {
    super(message);
  }
}
