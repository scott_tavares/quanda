package com.sct.quanda.service;

import com.sct.quanda.model.UserAccount;
import com.sct.quanda.model.Verificationtoken;
import com.sct.quanda.web.dto.UserDto;

/**
 *
 * @author stavares
 */
public interface UserAccountService {
  public void createVerificationToken(UserAccount userAccount, String token);
  public void deleteUserAccount(UserAccount findByEmail);
  public void saveUserAccount(UserAccount userAccount);
  public boolean emailExist(String email);
  public UserAccount findByEmail(String email);
  public UserAccount registerNewUserAccount(UserDto accountDto) throws EmailExistsException;
  public Verificationtoken getVerificationToken(String token);
  public Verificationtoken generateNewVerificationToken(String token);

  public UserAccount getUser(String token);
}
