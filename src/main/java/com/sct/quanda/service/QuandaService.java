package com.sct.quanda.service;

import com.sct.quanda.model.Quanda;
import java.util.List;

/**
 *
 * @author stavares
 */
public interface QuandaService {
  public void sendMessage();

  /**
   *
   * @return
   */
  public List<Quanda> listQuanda();
}
