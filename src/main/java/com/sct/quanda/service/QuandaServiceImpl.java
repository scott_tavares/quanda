package com.sct.quanda.service;

import com.sct.quanda.dao.QuandaDao;
import com.sct.quanda.model.Quanda;
import java.util.List;
import java.util.logging.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 *
 * @author stavares
 */
@Service
public class QuandaServiceImpl implements QuandaService {

  private static final Logger LOG = Logger
			.getLogger(QuandaServiceImpl.class.getName());

  @Autowired
  private QuandaDao quandaDao;

  @Override
  public void sendMessage() {
    LOG.info("************* QuandaServiceImpl **************");
  }

  @Override
  @Transactional
  @PreAuthorize("hasAuthority('ROLE_ADMIN')")
//  @Transactional(propagation = Propagation.REQUIRED, isolation = Isolation.DEFAULT, readOnly = false)
  public List<Quanda> listQuanda(){
    List<Quanda> quandaList = quandaDao.findAll();
    return quandaList;
  }
}
