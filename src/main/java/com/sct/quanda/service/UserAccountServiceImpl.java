package com.sct.quanda.service;

import com.sct.quanda.dao.UserAccountDao;
import com.sct.quanda.dao.VerificationtokenDao;
import com.sct.quanda.model.UserAccount;
import com.sct.quanda.model.Verificationtoken;
import com.sct.quanda.web.dto.UserDto;
import java.util.UUID;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 *
 * @author stavares
 */
@Service
public class UserAccountServiceImpl implements UserAccountService {

  private static final Logger LOG = Logger.getLogger(UserAccountServiceImpl.class.getName());

//  @Autowired
//  UserRepository userRepository;
  @Autowired
  UserAccountDao userAccountDao;

  @Autowired
  private VerificationtokenDao verificationtokenDao;

  @Autowired
  PasswordEncoder passwordEncoder;

  @Override
  public UserAccount registerNewUserAccount(UserDto accountDto)
          throws EmailExistsException {

    LOG.debug("Called UserAccountServiceImpl::registerNewUserAccount()");
    if (emailExist(accountDto.getEmail()))
      throw new EmailExistsException("There is an account with that email adress: "
              + accountDto.getEmail());
    LOG.debug("Called UserAccountServiceImpl::registerNewUserAccount::Checked for existing email!!!");

    UserAccount user = new UserAccount();
    user.setFirstname(accountDto.getFirstName());
    user.setLastname(accountDto.getLastName());
    user.setEmail(accountDto.getEmail());
    user.setPassword(passwordEncoder.encode(accountDto.getPassword()));
    user.setEnabled(Boolean.FALSE);
    user.setIsusing2fa(Boolean.FALSE);
    user.setSecret("");

    LOG.debug("Calling UserAccountServiceImpl::registerNewUserAccount::userAccountDao.create(user)");
    user = userAccountDao.create(user);
    LOG.debug("Returning from UserAccountServiceImpl::registerNewUserAccount::userAccountDao.create(user)");
    return user;

  }

  public boolean emailExist(String email) {
    try {
      UserAccount user = findByEmail(email);
    } catch (javax.persistence.NoResultException e) {
      return false;
    }
    return true;
  }

  @Override
  public UserAccount findByEmail(String email) {
    return userAccountDao.findByEmail(email);
  }

  @Override
  @Transactional
  public void createVerificationToken(UserAccount userAccount, String token) {
    final Verificationtoken myToken = new Verificationtoken(token, userAccount);
    verificationtokenDao.create(myToken);
  }

  @Override
  @Transactional
  public void deleteUserAccount(UserAccount userAccount) {
    userAccountDao.delete(userAccount);
  }

  @Override
  public Verificationtoken getVerificationToken(String token) {
    Verificationtoken verificationtoken;
    try {
      verificationtoken = verificationtokenDao.findByToken(token);
    } catch (javax.persistence.NoResultException e) {
      LOG.debug("UserAccountServiceImpl.getVerificationToken: "
              + token + " is NULL!!!");
      return null;
    }
    return verificationtoken;
  }

  @Override
  @Transactional
  public void saveUserAccount(UserAccount userAccount) {
    LOG.debug("UserAccountServiceImpl::saveUserAccount::userAccount.isEnabled: "
            + userAccount.isEnabled());
    userAccountDao.save(userAccount);
  }

  @Override
  public Verificationtoken generateNewVerificationToken(final String existingVerificationToken) {
    Verificationtoken verificationtoken =
            verificationtokenDao.findByToken(existingVerificationToken);
    String newToken = UUID.randomUUID().toString();
    verificationtoken.updateToken(newToken);
    verificationtoken = verificationtokenDao.save(verificationtoken);
    return verificationtoken;
  }

  @Override
  public UserAccount getUser(final String verificationToken) {
    final Verificationtoken token = verificationtokenDao.findByToken(verificationToken);
    if (token != null) {
      return token.getUserAccount();
    }
    return null;
  }
}
