package com.sct.quanda.model;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author stavares
 */
@Entity
@Table(name = "answer_multiple_choice")
@XmlRootElement
@NamedQueries({
  @NamedQuery(name = "AnswerMultipleChoice.findAll", query = "SELECT a FROM AnswerMultipleChoice a")})
public class AnswerMultipleChoice implements Serializable {

  private static final long serialVersionUID = 1L;
  @Id
  @GeneratedValue(strategy = GenerationType.IDENTITY)
  @Basic(optional = false)
  @Column(name = "idanswer_multiple_choice")
  private Integer idanswerMultipleChoice;
  @JoinColumn(name = "idanswer_multiple_choice", referencedColumnName = "idanswer", insertable = false, updatable = false)
  @OneToOne(optional = false)
  private Answer answer;

  public AnswerMultipleChoice() {
  }

  public AnswerMultipleChoice(Integer idanswerMultipleChoice) {
    this.idanswerMultipleChoice = idanswerMultipleChoice;
  }

  public Integer getIdanswerMultipleChoice() {
    return idanswerMultipleChoice;
  }

  public void setIdanswerMultipleChoice(Integer idanswerMultipleChoice) {
    this.idanswerMultipleChoice = idanswerMultipleChoice;
  }

  public Answer getAnswer() {
    return answer;
  }

  public void setAnswer(Answer answer) {
    this.answer = answer;
  }

  @Override
  public int hashCode() {
    int hash = 0;
    hash += (idanswerMultipleChoice != null ? idanswerMultipleChoice.hashCode() : 0);
    return hash;
  }

  @Override
  public boolean equals(Object object) {
    // TODO: Warning - this method won't work in the case the id fields are not set
    if (!(object instanceof AnswerMultipleChoice)) {
      return false;
    }
    AnswerMultipleChoice other = (AnswerMultipleChoice) object;
    if ((this.idanswerMultipleChoice == null && other.idanswerMultipleChoice != null) || (this.idanswerMultipleChoice != null && !this.idanswerMultipleChoice.equals(other.idanswerMultipleChoice)))
      return false;
    return true;
  }

  @Override
  public String toString() {
    return "com.sct.quanda.domain.quanda_acl.model.AnswerMultipleChoice[ idanswerMultipleChoice=" + idanswerMultipleChoice + " ]";
  }

}
