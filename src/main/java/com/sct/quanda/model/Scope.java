package com.sct.quanda.model;

import java.io.Serializable;
import java.util.Collection;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author stavares
 */
@Entity
@Table(name = "scope")
@XmlRootElement
@NamedQueries({
  @NamedQuery(name = "Scope.findAll", query = "SELECT s FROM Scope s")})
public class Scope implements Serializable {

  private static final long serialVersionUID = 1L;
  @Id
  @GeneratedValue(strategy = GenerationType.IDENTITY)
  @Basic(optional = false)
  @Column(name = "idscope")
  private Integer idscope;
  @Size(max = 45)
  @Column(name = "scope")
  private String scope;
  @OneToMany(cascade = CascadeType.ALL, mappedBy = "idscope")
  private Collection<QuestionGroup> questionGroupCollection;

  public Scope() {
  }

  public Scope(Integer idscope) {
    this.idscope = idscope;
  }

  public Integer getIdscope() {
    return idscope;
  }

  public void setIdscope(Integer idscope) {
    this.idscope = idscope;
  }

  public String getScope() {
    return scope;
  }

  public void setScope(String scope) {
    this.scope = scope;
  }

  @XmlTransient
  public Collection<QuestionGroup> getQuestionGroupCollection() {
    return questionGroupCollection;
  }

  public void setQuestionGroupCollection(Collection<QuestionGroup> questionGroupCollection) {
    this.questionGroupCollection = questionGroupCollection;
  }

  @Override
  public int hashCode() {
    int hash = 0;
    hash += (idscope != null ? idscope.hashCode() : 0);
    return hash;
  }

  @Override
  public boolean equals(Object object) {
    // TODO: Warning - this method won't work in the case the id fields are not set
    if (!(object instanceof Scope)) {
      return false;
    }
    Scope other = (Scope) object;
    if ((this.idscope == null && other.idscope != null) || (this.idscope != null && !this.idscope.equals(other.idscope)))
      return false;
    return true;
  }

  @Override
  public String toString() {
    return "com.sct.quanda.domain.quanda_acl.model.Scope[ idscope=" + idscope + " ]";
  }

}
