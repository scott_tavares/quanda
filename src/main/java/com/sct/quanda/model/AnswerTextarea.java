package com.sct.quanda.model;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author stavares
 */
@Entity
@Table(name = "answer_textarea")
@XmlRootElement
@NamedQueries({
  @NamedQuery(name = "AnswerTextarea.findAll", query = "SELECT a FROM AnswerTextarea a")})
public class AnswerTextarea implements Serializable {

  private static final long serialVersionUID = 1L;
  @Id
  @GeneratedValue(strategy = GenerationType.IDENTITY)
  @Basic(optional = false)
  @Column(name = "idanswer_textarea")
  private Integer idanswerTextarea;
  @JoinColumn(name = "idanswer_textarea", referencedColumnName = "idanswer", insertable = false, updatable = false)
  @OneToOne(optional = false)
  private Answer answer;

  public AnswerTextarea() {
  }

  public AnswerTextarea(Integer idanswerTextarea) {
    this.idanswerTextarea = idanswerTextarea;
  }

  public Integer getIdanswerTextarea() {
    return idanswerTextarea;
  }

  public void setIdanswerTextarea(Integer idanswerTextarea) {
    this.idanswerTextarea = idanswerTextarea;
  }

  public Answer getAnswer() {
    return answer;
  }

  public void setAnswer(Answer answer) {
    this.answer = answer;
  }

  @Override
  public int hashCode() {
    int hash = 0;
    hash += (idanswerTextarea != null ? idanswerTextarea.hashCode() : 0);
    return hash;
  }

  @Override
  public boolean equals(Object object) {
    // TODO: Warning - this method won't work in the case the id fields are not set
    if (!(object instanceof AnswerTextarea)) {
      return false;
    }
    AnswerTextarea other = (AnswerTextarea) object;
    if ((this.idanswerTextarea == null && other.idanswerTextarea != null) || (this.idanswerTextarea != null && !this.idanswerTextarea.equals(other.idanswerTextarea)))
      return false;
    return true;
  }

  @Override
  public String toString() {
    return "com.sct.quanda.domain.quanda_acl.model.AnswerTextarea[ idanswerTextarea=" + idanswerTextarea + " ]";
  }

}
