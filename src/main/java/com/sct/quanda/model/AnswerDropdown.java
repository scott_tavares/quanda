package com.sct.quanda.model;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author stavares
 */
@Entity
@Table(name = "answer_dropdown")
@XmlRootElement
@NamedQueries({
  @NamedQuery(name = "AnswerDropdown.findAll", query = "SELECT a FROM AnswerDropdown a")})
public class AnswerDropdown implements Serializable {

  private static final long serialVersionUID = 1L;
  @Id
  @GeneratedValue(strategy = GenerationType.IDENTITY)
  @Basic(optional = false)
  @Column(name = "idanswer_dropdown")
  private Integer idanswerDropdown;
  @JoinColumn(name = "idanswer_dropdown", referencedColumnName = "idanswer", insertable = false, updatable = false)
  @OneToOne(optional = false)
  private Answer answer;

  public AnswerDropdown() {
  }

  public AnswerDropdown(Integer idanswerDropdown) {
    this.idanswerDropdown = idanswerDropdown;
  }

  public Integer getIdanswerDropdown() {
    return idanswerDropdown;
  }

  public void setIdanswerDropdown(Integer idanswerDropdown) {
    this.idanswerDropdown = idanswerDropdown;
  }

  public Answer getAnswer() {
    return answer;
  }

  public void setAnswer(Answer answer) {
    this.answer = answer;
  }

  @Override
  public int hashCode() {
    int hash = 0;
    hash += (idanswerDropdown != null ? idanswerDropdown.hashCode() : 0);
    return hash;
  }

  @Override
  public boolean equals(Object object) {
    // TODO: Warning - this method won't work in the case the id fields are not set
    if (!(object instanceof AnswerDropdown)) {
      return false;
    }
    AnswerDropdown other = (AnswerDropdown) object;
    if ((this.idanswerDropdown == null && other.idanswerDropdown != null) || (this.idanswerDropdown != null && !this.idanswerDropdown.equals(other.idanswerDropdown)))
      return false;
    return true;
  }

  @Override
  public String toString() {
    return "com.sct.quanda.domain.quanda_acl.model.AnswerDropdown[ idanswerDropdown=" + idanswerDropdown + " ]";
  }

}
