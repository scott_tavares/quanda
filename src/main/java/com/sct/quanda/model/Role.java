package com.sct.quanda.model;

import java.io.Serializable;
import java.util.Collection;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author stavares
 */
@Entity
@Table(name = "role")
@XmlRootElement
@NamedQueries({
  @NamedQuery(name = "Role.findAll", query = "SELECT r FROM Role r")})
public class Role implements Serializable {

  private static final long serialVersionUID = 1L;
  @Id
  @GeneratedValue(strategy = GenerationType.IDENTITY)
  @Basic(optional = false)
  @Column(name = "ID")
  private Long id;
  @Size(max = 255)
  @Column(name = "NAME")
  private String name;

  @ManyToMany(mappedBy = "roleCollection")
  private Collection<UserAccount> userAccountCollection;

  @JoinTable(name = "roles_privileges",
          joinColumns
          = @JoinColumn(name = "role_id", referencedColumnName = "id"),
          inverseJoinColumns
          = @JoinColumn(name = "privilege_id", referencedColumnName = "id"))
  @ManyToMany
  private Collection<Privilege> privilegeCollection;

  public Role() {
  }

  public Role(Long id) {
    this.id = id;
  }

  public Long getId() {
    return id;
  }

  public void setId(Long id) {
    this.id = id;
  }

  public String getName() {
    return name;
  }

  public void setName(String name) {
    this.name = name;
  }

  @XmlTransient
  public Collection<UserAccount> getUserAccountCollection() {
    return userAccountCollection;
  }

  public void setUserAccountCollection(Collection<UserAccount> userAccountCollection) {
    this.userAccountCollection = userAccountCollection;
  }

  @XmlTransient
  public Collection<Privilege> getPrivilegeCollection() {
    return privilegeCollection;
  }

  public void setPrivilegeCollection(Collection<Privilege> privilegeCollection) {
    this.privilegeCollection = privilegeCollection;
  }

  @Override
  public int hashCode() {
    int hash = 0;
    hash += (id != null ? id.hashCode() : 0);
    return hash;
  }

  @Override
  public boolean equals(Object object) {
    // TODO: Warning - this method won't work in the case the id fields are not set
    if (!(object instanceof Role)) {
      return false;
    }
    Role other = (Role) object;
    if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id)))
      return false;
    return true;
  }

  @Override
  public String toString() {
    return "com.sct.quanda.domain.quanda_acl.model.Role[ id=" + id + " ]";
  }

}
