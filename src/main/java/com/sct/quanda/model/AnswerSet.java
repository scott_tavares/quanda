package com.sct.quanda.model;

import java.io.Serializable;
import java.util.Collection;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author stavares
 */
@Entity
@Table(name = "answer_set")
@XmlRootElement
@NamedQueries({
  @NamedQuery(name = "AnswerSet.findAll", query = "SELECT a FROM AnswerSet a")})
public class AnswerSet implements Serializable {

  private static final long serialVersionUID = 1L;
  @Id
  @GeneratedValue(strategy = GenerationType.IDENTITY)
  @Basic(optional = false)
  @Column(name = "idanswer_set")
  private Integer idanswerSet;
  @OneToMany(cascade = CascadeType.ALL, mappedBy = "idanswerSet")
  private Collection<Answer> answerCollection;
  @JoinColumn(name = "idanswer_type_list", referencedColumnName = "idanswer_type_list")
  @ManyToOne(optional = false)
  private AnswerTypeList idanswerTypeList;
  @JoinColumn(name = "iidquanda", referencedColumnName = "idquanda")
  @ManyToOne(optional = false)
  private Quanda iidquanda;

  public AnswerSet() {
  }

  public AnswerSet(Integer idanswerSet) {
    this.idanswerSet = idanswerSet;
  }

  public Integer getIdanswerSet() {
    return idanswerSet;
  }

  public void setIdanswerSet(Integer idanswerSet) {
    this.idanswerSet = idanswerSet;
  }

  @XmlTransient
  public Collection<Answer> getAnswerCollection() {
    return answerCollection;
  }

  public void setAnswerCollection(Collection<Answer> answerCollection) {
    this.answerCollection = answerCollection;
  }

  public AnswerTypeList getIdanswerTypeList() {
    return idanswerTypeList;
  }

  public void setIdanswerTypeList(AnswerTypeList idanswerTypeList) {
    this.idanswerTypeList = idanswerTypeList;
  }

  public Quanda getIidquanda() {
    return iidquanda;
  }

  public void setIidquanda(Quanda iidquanda) {
    this.iidquanda = iidquanda;
  }

  @Override
  public int hashCode() {
    int hash = 0;
    hash += (idanswerSet != null ? idanswerSet.hashCode() : 0);
    return hash;
  }

  @Override
  public boolean equals(Object object) {
    // TODO: Warning - this method won't work in the case the id fields are not set
    if (!(object instanceof AnswerSet)) {
      return false;
    }
    AnswerSet other = (AnswerSet) object;
    if ((this.idanswerSet == null && other.idanswerSet != null) || (this.idanswerSet != null && !this.idanswerSet.equals(other.idanswerSet)))
      return false;
    return true;
  }

  @Override
  public String toString() {
    return "com.sct.quanda.domain.quanda_acl.model.AnswerSet[ idanswerSet=" + idanswerSet + " ]";
  }

}
