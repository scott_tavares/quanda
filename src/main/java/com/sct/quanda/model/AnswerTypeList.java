package com.sct.quanda.model;

import java.io.Serializable;
import java.util.Collection;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author stavares
 */
@Entity
@Table(name = "answer_type_list")
@XmlRootElement
@NamedQueries({
  @NamedQuery(name = "AnswerTypeList.findAll", query = "SELECT a FROM AnswerTypeList a")})
public class AnswerTypeList implements Serializable {

  private static final long serialVersionUID = 1L;
  @Id
  @GeneratedValue(strategy = GenerationType.IDENTITY)
  @Basic(optional = false)
  @Column(name = "idanswer_type_list")
  private Integer idanswerTypeList;
  @Size(max = 45)
  @Column(name = "answer_type_name")
  private String answerTypeName;
  @OneToMany(cascade = CascadeType.ALL, mappedBy = "idanswerTypeList")
  private Collection<AnswerSet> answerSetCollection;

  public AnswerTypeList() {
  }

  public AnswerTypeList(Integer idanswerTypeList) {
    this.idanswerTypeList = idanswerTypeList;
  }

  public Integer getIdanswerTypeList() {
    return idanswerTypeList;
  }

  public void setIdanswerTypeList(Integer idanswerTypeList) {
    this.idanswerTypeList = idanswerTypeList;
  }

  public String getAnswerTypeName() {
    return answerTypeName;
  }

  public void setAnswerTypeName(String answerTypeName) {
    this.answerTypeName = answerTypeName;
  }

  @XmlTransient
  public Collection<AnswerSet> getAnswerSetCollection() {
    return answerSetCollection;
  }

  public void setAnswerSetCollection(Collection<AnswerSet> answerSetCollection) {
    this.answerSetCollection = answerSetCollection;
  }

  @Override
  public int hashCode() {
    int hash = 0;
    hash += (idanswerTypeList != null ? idanswerTypeList.hashCode() : 0);
    return hash;
  }

  @Override
  public boolean equals(Object object) {
    // TODO: Warning - this method won't work in the case the id fields are not set
    if (!(object instanceof AnswerTypeList)) {
      return false;
    }
    AnswerTypeList other = (AnswerTypeList) object;
    if ((this.idanswerTypeList == null && other.idanswerTypeList != null) || (this.idanswerTypeList != null && !this.idanswerTypeList.equals(other.idanswerTypeList)))
      return false;
    return true;
  }

  @Override
  public String toString() {
    return "com.sct.quanda.domain.quanda_acl.model.AnswerTypeList[ idanswerTypeList=" + idanswerTypeList + " ]";
  }

}
