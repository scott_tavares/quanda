package com.sct.quanda.model;

import java.io.Serializable;
import java.util.Collection;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author stavares
 */
@Entity
@Table(name = "question")
@XmlRootElement
@NamedQueries({
  @NamedQuery(name = "Question.findAll", query = "SELECT q FROM Question q")})
public class Question implements Serializable {

  private static final long serialVersionUID = 1L;
  @Id
  @GeneratedValue(strategy = GenerationType.IDENTITY)
  @Basic(optional = false)
  @Column(name = "idquestion")
  private Integer idquestion;
  @Size(max = 4096)
  @Column(name = "question_text")
  private String questionText;
  @JoinColumn(name = "question_group_id", referencedColumnName = "idquestion-group")
  @ManyToOne(optional = false)
  private QuestionGroup questionGroupId;
  @JoinColumn(name = "idquestion_type", referencedColumnName = "idquestion_type")
  @ManyToOne(optional = false)
  private QuestionType idquestionType;
  @OneToMany(cascade = CascadeType.ALL, mappedBy = "questionIdquestion")
  private Collection<QuestionItem> questionItemCollection;

  public Question() {
  }

  public Question(Integer idquestion) {
    this.idquestion = idquestion;
  }

  public Integer getIdquestion() {
    return idquestion;
  }

  public void setIdquestion(Integer idquestion) {
    this.idquestion = idquestion;
  }

  public String getQuestionText() {
    return questionText;
  }

  public void setQuestionText(String questionText) {
    this.questionText = questionText;
  }

  public QuestionGroup getQuestionGroupId() {
    return questionGroupId;
  }

  public void setQuestionGroupId(QuestionGroup questionGroupId) {
    this.questionGroupId = questionGroupId;
  }

  public QuestionType getIdquestionType() {
    return idquestionType;
  }

  public void setIdquestionType(QuestionType idquestionType) {
    this.idquestionType = idquestionType;
  }

  @XmlTransient
  public Collection<QuestionItem> getQuestionItemCollection() {
    return questionItemCollection;
  }

  public void setQuestionItemCollection(Collection<QuestionItem> questionItemCollection) {
    this.questionItemCollection = questionItemCollection;
  }

  @Override
  public int hashCode() {
    int hash = 0;
    hash += (idquestion != null ? idquestion.hashCode() : 0);
    return hash;
  }

  @Override
  public boolean equals(Object object) {
    // TODO: Warning - this method won't work in the case the id fields are not set
    if (!(object instanceof Question)) {
      return false;
    }
    Question other = (Question) object;
    if ((this.idquestion == null && other.idquestion != null) || (this.idquestion != null && !this.idquestion.equals(other.idquestion)))
      return false;
    return true;
  }

  @Override
  public String toString() {
    return "com.sct.quanda.domain.quanda_acl.model.Question[ idquestion=" + idquestion + " ]";
  }

}
