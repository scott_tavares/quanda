package com.sct.quanda.model;

import java.io.Serializable;
import java.util.Collection;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author stavares
 */
@Entity
@Table(name = "question_group")
@XmlRootElement
@NamedQueries({
  @NamedQuery(name = "QuestionGroup.findAll", query = "SELECT q FROM QuestionGroup q")})
public class QuestionGroup implements Serializable {

  private static final long serialVersionUID = 1L;
  @Id
  @GeneratedValue(strategy = GenerationType.IDENTITY)
  @Basic(optional = false)
  @Column(name = "idquestion-group")
  private Integer idquestionGroup;
  @Basic(optional = false)
  @NotNull
  @Size(min = 1, max = 45)
  @Column(name = "title")
  private String title;
  @JoinTable(name = "question_group_has_question_group", joinColumns = {
    @JoinColumn(name = "idquestion_group", referencedColumnName = "idquestion-group")}, inverseJoinColumns = {
    @JoinColumn(name = "idquestion_group_sub", referencedColumnName = "idquestion-group")})
  @ManyToMany
  private Collection<QuestionGroup> questionGroupCollection;
  @ManyToMany(mappedBy = "questionGroupCollection")
  private Collection<QuestionGroup> questionGroupCollection1;
  @OneToMany(cascade = CascadeType.ALL, mappedBy = "questionGroupId")
  private Collection<Question> questionCollection;
  @JoinColumn(name = "idquanda", referencedColumnName = "idquanda")
  @ManyToOne(optional = false)
  private Quanda idquanda;
  @JoinColumn(name = "idscope", referencedColumnName = "idscope")
  @ManyToOne(optional = false)
  private Scope idscope;

  public QuestionGroup() {
  }

  public QuestionGroup(Integer idquestionGroup) {
    this.idquestionGroup = idquestionGroup;
  }

  public QuestionGroup(Integer idquestionGroup, String title) {
    this.idquestionGroup = idquestionGroup;
    this.title = title;
  }

  public Integer getIdquestionGroup() {
    return idquestionGroup;
  }

  public void setIdquestionGroup(Integer idquestionGroup) {
    this.idquestionGroup = idquestionGroup;
  }

  public String getTitle() {
    return title;
  }

  public void setTitle(String title) {
    this.title = title;
  }

  @XmlTransient
  public Collection<QuestionGroup> getQuestionGroupCollection() {
    return questionGroupCollection;
  }

  public void setQuestionGroupCollection(Collection<QuestionGroup> questionGroupCollection) {
    this.questionGroupCollection = questionGroupCollection;
  }

  @XmlTransient
  public Collection<QuestionGroup> getQuestionGroupCollection1() {
    return questionGroupCollection1;
  }

  public void setQuestionGroupCollection1(Collection<QuestionGroup> questionGroupCollection1) {
    this.questionGroupCollection1 = questionGroupCollection1;
  }

  @XmlTransient
  public Collection<Question> getQuestionCollection() {
    return questionCollection;
  }

  public void setQuestionCollection(Collection<Question> questionCollection) {
    this.questionCollection = questionCollection;
  }

  public Quanda getIdquanda() {
    return idquanda;
  }

  public void setIdquanda(Quanda idquanda) {
    this.idquanda = idquanda;
  }

  public Scope getIdscope() {
    return idscope;
  }

  public void setIdscope(Scope idscope) {
    this.idscope = idscope;
  }

  @Override
  public int hashCode() {
    int hash = 0;
    hash += (idquestionGroup != null ? idquestionGroup.hashCode() : 0);
    return hash;
  }

  @Override
  public boolean equals(Object object) {
    // TODO: Warning - this method won't work in the case the id fields are not set
    if (!(object instanceof QuestionGroup)) {
      return false;
    }
    QuestionGroup other = (QuestionGroup) object;
    if ((this.idquestionGroup == null && other.idquestionGroup != null) || (this.idquestionGroup != null && !this.idquestionGroup.equals(other.idquestionGroup)))
      return false;
    return true;
  }

  @Override
  public String toString() {
    return "com.sct.quanda.domain.quanda_acl.model.QuestionGroup[ idquestionGroup=" + idquestionGroup + " ]";
  }

}
