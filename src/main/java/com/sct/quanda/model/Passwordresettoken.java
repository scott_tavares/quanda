package com.sct.quanda.model;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author stavares
 */
@Entity
@Table(name = "passwordresettoken")
@XmlRootElement
@NamedQueries({
  @NamedQuery(name = "Passwordresettoken.findAll", query = "SELECT p FROM Passwordresettoken p")})
public class Passwordresettoken implements Serializable {

  private static final long serialVersionUID = 1L;
  @Id
  @GeneratedValue(strategy = GenerationType.IDENTITY)
  @Basic(optional = false)
  @Column(name = "ID")
  private Long id;
  @Column(name = "EXPIRYDATE")
  @Temporal(TemporalType.DATE)
  private Date expirydate;
  @Size(max = 255)
  @Column(name = "TOKEN")
  private String token;

//  @ManyToOne(optional = false)
  @OneToOne(targetEntity = UserAccount.class, fetch = FetchType.EAGER)
  @JoinColumn(name = "user_id", referencedColumnName = "ID")
  private UserAccount userAccount;

  public Passwordresettoken() {
  }

  public Passwordresettoken(Long id) {
    this.id = id;
  }

  public Long getId() {
    return id;
  }

  public void setId(Long id) {
    this.id = id;
  }

  public Date getExpirydate() {
    return expirydate;
  }

  public void setExpirydate(Date expirydate) {
    this.expirydate = expirydate;
  }

  public String getToken() {
    return token;
  }

  public void setToken(String token) {
    this.token = token;
  }

  public UserAccount getUserAccount() {
    return userAccount;
  }

  public void setUserAccount(UserAccount userAccount) {
    this.userAccount = userAccount;
  }

  @Override
  public int hashCode() {
    int hash = 0;
    hash += (id != null ? id.hashCode() : 0);
    return hash;
  }

  @Override
  public boolean equals(Object object) {
    // TODO: Warning - this method won't work in the case the id fields are not set
    if (!(object instanceof Passwordresettoken)) {
      return false;
    }
    Passwordresettoken other = (Passwordresettoken) object;
    if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id)))
      return false;
    return true;
  }

  @Override
  public String toString() {
    return "com.sct.quanda.domain.quanda_acl.model.Passwordresettoken[ id=" + id + " ]";
  }

}
