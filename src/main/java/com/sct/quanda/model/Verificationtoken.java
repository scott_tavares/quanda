package com.sct.quanda.model;

import java.io.Serializable;
import java.util.Calendar;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;
import org.apache.log4j.Logger;

/**
 *
 * @author stavares
 */
@Entity
@Table(name = "verificationtoken")
@XmlRootElement
//@NamedQueries({
//  @NamedQuery(name = "Verificationtoken.findAll", query = "SELECT v FROM Verificationtoken v")})
public class Verificationtoken implements Serializable {

  private static final Logger LOG = Logger.getLogger(Verificationtoken.class.getName());

  private static final int EXPIRATION = 60 * 24;

  @JoinColumn(nullable = false, name = "user_id", referencedColumnName = "ID")
  //@ManyToOne(optional = false)
  @OneToOne(targetEntity = UserAccount.class, fetch = FetchType.EAGER)
  private UserAccount userAccount;

  private static final long serialVersionUID = 1L;
  @Id
  @GeneratedValue(strategy = GenerationType.IDENTITY)
  @Basic(optional = false)
  @Column(name = "ID")
  private Long id;

  @Column(name = "EXPIRYDATE")
  @Temporal(TemporalType.DATE)
  private Date expirydate;

  @Size(max = 255)
  @Column(name = "TOKEN")
  private String token;

  public Verificationtoken() {
    super();
  }

  public Verificationtoken(Long id) {
    this.id = id;
  }

  public Verificationtoken(final String token) {
    super();
    this.token = token;
    this.expirydate = calculateExpiryDate(EXPIRATION);
  }

  public Verificationtoken(String token, UserAccount userAccount) {
    super();
    this.token = token;
    this.userAccount = userAccount;
    this.expirydate = calculateExpiryDate(EXPIRATION);
  }

  public Long getId() {
    return id;
  }

  public void setId(Long id) {
    this.id = id;
  }

  public Date getExpirydate() {
    return expirydate;
  }

  public void setExpirydate(Date expirydate) {
    this.expirydate = expirydate;
  }

  public String getToken() {
    return token;
  }

  public void setToken(String token) {
    this.token = token;
  }

  @Override
  public int hashCode() {
    int hash = 0;
    hash += (id != null ? id.hashCode() : 0);
    return hash;
  }

  @Override
  public boolean equals(Object object) {
    // TODO: Warning - this method won't work in the case the id fields are not set
    if (!(object instanceof Verificationtoken)) {
      return false;
    }
    Verificationtoken other = (Verificationtoken) object;
    if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id)))
      return false;
    return true;
  }

  @Override
  public String toString() {
    return "com.sct.quanda.domain.quanda_acl.model.Verificationtoken[ id=" + id + " ]";
  }

  public UserAccount getUserAccount() {
    return userAccount;
  }

  public void setUserAccount(UserAccount userAccount) {
    this.userAccount = userAccount;
  }

  private Date calculateExpiryDate(final int expiryTimeInMinutes) {
    final Calendar cal = Calendar.getInstance();
    cal.setTimeInMillis(new Date().getTime());
    cal.add(Calendar.MINUTE, expiryTimeInMinutes);
    return new Date(cal.getTime().getTime());
  }

  public void updateToken(String newtoken) {
    this.token = newtoken;
    this.expirydate = calculateExpiryDate(EXPIRATION);
  }
}
