package com.sct.quanda.model;

import java.io.Serializable;
import java.util.Collection;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author stavares
 */
@Entity
@Table(name = "question_type")
@XmlRootElement
@NamedQueries({
  @NamedQuery(name = "QuestionType.findAll", query = "SELECT q FROM QuestionType q")})
public class QuestionType implements Serializable {

  private static final long serialVersionUID = 1L;
  @Id
  @GeneratedValue(strategy = GenerationType.IDENTITY)
  @Basic(optional = false)
  @Column(name = "idquestion_type")
  private Integer idquestionType;
  @Size(max = 45)
  @Column(name = "question_type_name")
  private String questionTypeName;
  @OneToMany(cascade = CascadeType.ALL, mappedBy = "idquestionType")
  private Collection<Question> questionCollection;

  public QuestionType() {
  }

  public QuestionType(Integer idquestionType) {
    this.idquestionType = idquestionType;
  }

  public Integer getIdquestionType() {
    return idquestionType;
  }

  public void setIdquestionType(Integer idquestionType) {
    this.idquestionType = idquestionType;
  }

  public String getQuestionTypeName() {
    return questionTypeName;
  }

  public void setQuestionTypeName(String questionTypeName) {
    this.questionTypeName = questionTypeName;
  }

  @XmlTransient
  public Collection<Question> getQuestionCollection() {
    return questionCollection;
  }

  public void setQuestionCollection(Collection<Question> questionCollection) {
    this.questionCollection = questionCollection;
  }

  @Override
  public int hashCode() {
    int hash = 0;
    hash += (idquestionType != null ? idquestionType.hashCode() : 0);
    return hash;
  }

  @Override
  public boolean equals(Object object) {
    // TODO: Warning - this method won't work in the case the id fields are not set
    if (!(object instanceof QuestionType)) {
      return false;
    }
    QuestionType other = (QuestionType) object;
    if ((this.idquestionType == null && other.idquestionType != null) || (this.idquestionType != null && !this.idquestionType.equals(other.idquestionType)))
      return false;
    return true;
  }

  @Override
  public String toString() {
    return "com.sct.quanda.domain.quanda_acl.model.QuestionType[ idquestionType=" + idquestionType + " ]";
  }

}
