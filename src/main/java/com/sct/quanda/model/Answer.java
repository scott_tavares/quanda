package com.sct.quanda.model;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author stavares
 */
@Entity
@Table(name = "answer")
@XmlRootElement
@NamedQueries({
  @NamedQuery(name = "Answer.findAll", query = "SELECT a FROM Answer a")})
public class Answer implements Serializable {

  private static final long serialVersionUID = 1L;
  @Id
  @GeneratedValue(strategy = GenerationType.IDENTITY)
  @Basic(optional = false)
  @Column(name = "idanswer")
  private Integer idanswer;
  @OneToOne(cascade = CascadeType.ALL, mappedBy = "answer")
  private AnswerDropdown answerDropdown;
  @OneToOne(cascade = CascadeType.ALL, mappedBy = "answer")
  private AnswerMultipleGuess answerMultipleGuess;
  @OneToOne(cascade = CascadeType.ALL, mappedBy = "answer")
  private AnswerMultipleChoice answerMultipleChoice;
  @JoinColumn(name = "idanswer_set", referencedColumnName = "idanswer_set")
  @ManyToOne(optional = false)
  private AnswerSet idanswerSet;
  @OneToOne(cascade = CascadeType.ALL, mappedBy = "answer")
  private AnswerTextarea answerTextarea;

  public Answer() {
  }

  public Answer(Integer idanswer) {
    this.idanswer = idanswer;
  }

  public Integer getIdanswer() {
    return idanswer;
  }

  public void setIdanswer(Integer idanswer) {
    this.idanswer = idanswer;
  }

  public AnswerDropdown getAnswerDropdown() {
    return answerDropdown;
  }

  public void setAnswerDropdown(AnswerDropdown answerDropdown) {
    this.answerDropdown = answerDropdown;
  }

  public AnswerMultipleGuess getAnswerMultipleGuess() {
    return answerMultipleGuess;
  }

  public void setAnswerMultipleGuess(AnswerMultipleGuess answerMultipleGuess) {
    this.answerMultipleGuess = answerMultipleGuess;
  }

  public AnswerMultipleChoice getAnswerMultipleChoice() {
    return answerMultipleChoice;
  }

  public void setAnswerMultipleChoice(AnswerMultipleChoice answerMultipleChoice) {
    this.answerMultipleChoice = answerMultipleChoice;
  }

  public AnswerSet getIdanswerSet() {
    return idanswerSet;
  }

  public void setIdanswerSet(AnswerSet idanswerSet) {
    this.idanswerSet = idanswerSet;
  }

  public AnswerTextarea getAnswerTextarea() {
    return answerTextarea;
  }

  public void setAnswerTextarea(AnswerTextarea answerTextarea) {
    this.answerTextarea = answerTextarea;
  }

  @Override
  public int hashCode() {
    int hash = 0;
    hash += (idanswer != null ? idanswer.hashCode() : 0);
    return hash;
  }

  @Override
  public boolean equals(Object object) {
    // TODO: Warning - this method won't work in the case the id fields are not set
    if (!(object instanceof Answer)) {
      return false;
    }
    Answer other = (Answer) object;
    if ((this.idanswer == null && other.idanswer != null) || (this.idanswer != null && !this.idanswer.equals(other.idanswer)))
      return false;
    return true;
  }

  @Override
  public String toString() {
    return "com.sct.quanda.domain.quanda_acl.model.Answer[ idanswer=" + idanswer + " ]";
  }

}
