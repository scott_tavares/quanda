package com.sct.quanda.model;

import java.io.Serializable;
import java.util.Collection;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author stavares
 */
@Entity
@Table(name = "quanda")
@XmlRootElement
@NamedQueries({
  @NamedQuery(name = "Quanda.findAll", query = "SELECT q FROM Quanda q")})
public class Quanda implements Serializable {

  private static final long serialVersionUID = 1L;
  @Id
  @GeneratedValue(strategy = GenerationType.IDENTITY)
  @Basic(optional = false)
  @Column(name = "idquanda")
  private Integer idquanda;
  @Basic(optional = false)
  @NotNull
  @Size(min = 1, max = 45)
  @Column(name = "title")
  private String title;
  @OneToMany(cascade = CascadeType.ALL, mappedBy = "idquanda")
  private Collection<QuestionGroup> questionGroupCollection;
  @OneToMany(cascade = CascadeType.ALL, mappedBy = "iidquanda")
  private Collection<AnswerSet> answerSetCollection;

  public Quanda() {
  }

  public Quanda(Integer idquanda) {
    this.idquanda = idquanda;
  }

  public Quanda(Integer idquanda, String title) {
    this.idquanda = idquanda;
    this.title = title;
  }

  public Integer getIdquanda() {
    return idquanda;
  }

  public void setIdquanda(Integer idquanda) {
    this.idquanda = idquanda;
  }

  public String getTitle() {
    return title;
  }

  public void setTitle(String title) {
    this.title = title;
  }

  @XmlTransient
  public Collection<QuestionGroup> getQuestionGroupCollection() {
    return questionGroupCollection;
  }

  public void setQuestionGroupCollection(Collection<QuestionGroup> questionGroupCollection) {
    this.questionGroupCollection = questionGroupCollection;
  }

  @XmlTransient
  public Collection<AnswerSet> getAnswerSetCollection() {
    return answerSetCollection;
  }

  public void setAnswerSetCollection(Collection<AnswerSet> answerSetCollection) {
    this.answerSetCollection = answerSetCollection;
  }

  @Override
  public int hashCode() {
    int hash = 0;
    hash += (idquanda != null ? idquanda.hashCode() : 0);
    return hash;
  }

  @Override
  public boolean equals(Object object) {
    // TODO: Warning - this method won't work in the case the id fields are not set
    if (!(object instanceof Quanda)) {
      return false;
    }
    Quanda other = (Quanda) object;
    if ((this.idquanda == null && other.idquanda != null) || (this.idquanda != null && !this.idquanda.equals(other.idquanda)))
      return false;
    return true;
  }

  @Override
  public String toString() {
    return "com.sct.quanda.domain.quanda_acl.model.Quanda[ idquanda=" + idquanda + " ]";
  }

}
