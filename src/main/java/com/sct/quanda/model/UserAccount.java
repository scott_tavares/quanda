package com.sct.quanda.model;

import java.io.Serializable;
import java.util.Collection;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.validation.constraints.Pattern;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author stavares
 */
@Entity
@Table(name = "user_account")
@XmlRootElement
@NamedQueries({
  @NamedQuery(name = "UserAccount.findAll", query = "SELECT u FROM UserAccount u")})
public class UserAccount implements Serializable {

  private static final long serialVersionUID = 1L;

  @Id
  @GeneratedValue(strategy = GenerationType.IDENTITY)
  @Basic(optional = false)
  @Column(name = "ID")
  private Long id;

  @Size(max = 255)
  @Column(name = "EMAIL")
  @Pattern(regexp="[a-z0-9!#$%&'*+/=?^_`{|}~-]+(?:\\.[a-z0-9!#$%&'*+/=?^_`{|}~-]+)*@(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?", message="Invalid email")//if the field contains email address consider using this annotation to enforce field validation
  private String email;

  @Column(name = "ENABLED")
  private Boolean enabled;

  @Size(max = 255)
  @Column(name = "FIRSTNAME")
  private String firstname;

  @Column(name = "ISUSING2FA")
  private Boolean isusing2fa;

  @Size(max = 255)
  @Column(name = "LASTNAME")
  private String lastname;

  @Size(max = 240)
  @Column(name = "PASSWORD")
  private String password;

  @Size(max = 255)
  @Column(name = "SECRET")
  private String secret;

  @ManyToMany(fetch = FetchType.EAGER)
  @JoinTable(name = "users_roles", joinColumns = @JoinColumn(name = "user_id", referencedColumnName = "id"), inverseJoinColumns = @JoinColumn(name = "role_id", referencedColumnName = "id"))
  private Collection<Role> roleCollection;

  @OneToMany(cascade = CascadeType.ALL, mappedBy = "userAccount")
  private Collection<Verificationtoken> verificationtokenCollection;

  public UserAccount() {
    super();
    this.enabled=false;
  }

  public UserAccount(Long id) {
    super();
    this.id = id;
    this.enabled=false;
  }

  public Long getId() {
    return id;
  }

  public void setId(Long id) {
    this.id = id;
  }

  public String getEmail() {
    return email;
  }

  public void setEmail(String email) {
    this.email = email;
  }

  public Boolean getEnabled() {
    return enabled;
  }

  public void setEnabled(Boolean enabled) {
    this.enabled = enabled;
  }

  public String getFirstname() {
    return firstname;
  }

  public void setFirstname(String firstname) {
    this.firstname = firstname;
  }

  public Boolean getIsusing2fa() {
    return isusing2fa;
  }

  public void setIsusing2fa(Boolean isusing2fa) {
    this.isusing2fa = isusing2fa;
  }

  public String getLastname() {
    return lastname;
  }

  public void setLastname(String lastname) {
    this.lastname = lastname;
  }

  public String getPassword() {
    return password;
  }

  public void setPassword(String password) {
    this.password = password;
  }

  public String getSecret() {
    return secret;
  }

  public void setSecret(String secret) {
    this.secret = secret;
  }

  @XmlTransient
  public Collection<Role> getRoleCollection() {
    return roleCollection;
  }

  public void setRoleCollection(Collection<Role> roleCollection) {
    this.roleCollection = roleCollection;
  }

//  @XmlTransient
//  public Collection<Passwordresettoken> getPasswordresettokenCollection() {
//    return passwordresettokenCollection;
//  }
//
//  public void setPasswordresettokenCollection(Collection<Passwordresettoken> passwordresettokenCollection) {
//    this.passwordresettokenCollection = passwordresettokenCollection;
//  }

  @Override
  public int hashCode() {
    int hash = 0;
    hash += (id != null ? id.hashCode() : 0);
    return hash;
  }

  @Override
  public boolean equals(Object object) {
    // TODO: Warning - this method won't work in the case the id fields are not set
    if (!(object instanceof UserAccount)) {
      return false;
    }
    UserAccount other = (UserAccount) object;
    if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id)))
      return false;
    return true;
  }

  @Override
  public String toString() {
    return "com.sct.quanda.domain.quanda_acl.model.UserAccount[ id=" + id + " ]";
  }

  @XmlTransient
  public Collection<Verificationtoken> getVerificationtokenCollection() {
    return verificationtokenCollection;
  }

  public void setVerificationtokenCollection(Collection<Verificationtoken> verificationtokenCollection) {
    this.verificationtokenCollection = verificationtokenCollection;
  }

  public boolean isEnabled(){
    return this.enabled;
  }
}
