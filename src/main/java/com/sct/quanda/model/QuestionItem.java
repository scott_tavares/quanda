package com.sct.quanda.model;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author stavares
 */
@Entity
@Table(name = "question_item")
@XmlRootElement
@NamedQueries({
  @NamedQuery(name = "QuestionItem.findAll", query = "SELECT q FROM QuestionItem q")})
public class QuestionItem implements Serializable {

  private static final long serialVersionUID = 1L;
  @Id
  @GeneratedValue(strategy = GenerationType.IDENTITY)
  @Basic(optional = false)
  @Column(name = "idquestion_item")
  private Integer idquestionItem;
  @Size(max = 255)
  @Column(name = "question_item_text")
  private String questionItemText;
  @JoinColumn(name = "question_idquestion", referencedColumnName = "idquestion")
  @ManyToOne(optional = false)
  private Question questionIdquestion;

  public QuestionItem() {
  }

  public QuestionItem(Integer idquestionItem) {
    this.idquestionItem = idquestionItem;
  }

  public Integer getIdquestionItem() {
    return idquestionItem;
  }

  public void setIdquestionItem(Integer idquestionItem) {
    this.idquestionItem = idquestionItem;
  }

  public String getQuestionItemText() {
    return questionItemText;
  }

  public void setQuestionItemText(String questionItemText) {
    this.questionItemText = questionItemText;
  }

  public Question getQuestionIdquestion() {
    return questionIdquestion;
  }

  public void setQuestionIdquestion(Question questionIdquestion) {
    this.questionIdquestion = questionIdquestion;
  }

  @Override
  public int hashCode() {
    int hash = 0;
    hash += (idquestionItem != null ? idquestionItem.hashCode() : 0);
    return hash;
  }

  @Override
  public boolean equals(Object object) {
    // TODO: Warning - this method won't work in the case the id fields are not set
    if (!(object instanceof QuestionItem)) {
      return false;
    }
    QuestionItem other = (QuestionItem) object;
    if ((this.idquestionItem == null && other.idquestionItem != null) || (this.idquestionItem != null && !this.idquestionItem.equals(other.idquestionItem)))
      return false;
    return true;
  }

  @Override
  public String toString() {
    return "com.sct.quanda.domain.quanda_acl.model.QuestionItem[ idquestionItem=" + idquestionItem + " ]";
  }

}
