package com.sct.quanda.model;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author stavares
 */
@Entity
@Table(name = "answer_multiple_guess")
@XmlRootElement
@NamedQueries({
  @NamedQuery(name = "AnswerMultipleGuess.findAll", query = "SELECT a FROM AnswerMultipleGuess a")})
public class AnswerMultipleGuess implements Serializable {

  private static final long serialVersionUID = 1L;
  @Id
  @GeneratedValue(strategy = GenerationType.IDENTITY)
  @Basic(optional = false)
  @Column(name = "idanswer_multiple_guess")
  private Integer idanswerMultipleGuess;
  @JoinColumn(name = "idanswer_multiple_guess", referencedColumnName = "idanswer", insertable = false, updatable = false)
  @OneToOne(optional = false)
  private Answer answer;

  public AnswerMultipleGuess() {
  }

  public AnswerMultipleGuess(Integer idanswerMultipleGuess) {
    this.idanswerMultipleGuess = idanswerMultipleGuess;
  }

  public Integer getIdanswerMultipleGuess() {
    return idanswerMultipleGuess;
  }

  public void setIdanswerMultipleGuess(Integer idanswerMultipleGuess) {
    this.idanswerMultipleGuess = idanswerMultipleGuess;
  }

  public Answer getAnswer() {
    return answer;
  }

  public void setAnswer(Answer answer) {
    this.answer = answer;
  }

  @Override
  public int hashCode() {
    int hash = 0;
    hash += (idanswerMultipleGuess != null ? idanswerMultipleGuess.hashCode() : 0);
    return hash;
  }

  @Override
  public boolean equals(Object object) {
    // TODO: Warning - this method won't work in the case the id fields are not set
    if (!(object instanceof AnswerMultipleGuess)) {
      return false;
    }
    AnswerMultipleGuess other = (AnswerMultipleGuess) object;
    if ((this.idanswerMultipleGuess == null && other.idanswerMultipleGuess != null) || (this.idanswerMultipleGuess != null && !this.idanswerMultipleGuess.equals(other.idanswerMultipleGuess)))
      return false;
    return true;
  }

  @Override
  public String toString() {
    return "com.sct.quanda.domain.quanda_acl.model.AnswerMultipleGuess[ idanswerMultipleGuess=" + idanswerMultipleGuess + " ]";
  }

}
