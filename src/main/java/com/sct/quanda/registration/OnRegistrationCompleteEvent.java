package com.sct.quanda.registration;

import com.sct.quanda.model.UserAccount;
import com.sct.quanda.model.Verificationtoken;
import java.util.Locale;
import org.springframework.context.ApplicationEvent;

/**
 *
 * @author stavares
 */
public class OnRegistrationCompleteEvent extends ApplicationEvent {

  private final String verificationUrl;
  private final Locale locale;
  private final UserAccount userAccount;
  private Verificationtoken verificationtoken;

  public OnRegistrationCompleteEvent(
          final UserAccount userAccount,
          final Verificationtoken verificationtoken,
          final String verificationUrl,
          final Locale locale) {

    super(userAccount);
    this.userAccount = userAccount;
    this.verificationtoken = verificationtoken;
    this.verificationUrl = verificationUrl;
    this.locale = locale;
  }

  /**
   * @return the appUrl
   */
  public String getVerificationUrl() {
    return verificationUrl;
  }

  /**
   * @return the locale
   */
  public Locale getLocale() {
    return locale;
  }

  /**
   * @return the userAccount
   */
  public UserAccount getUserAccount() {
    return userAccount;
  }

  /**
   * @return the verificationtoken
   */
  public Verificationtoken getVerificationtoken() {
    return verificationtoken;
  }
}
