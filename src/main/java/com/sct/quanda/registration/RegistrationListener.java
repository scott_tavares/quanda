package com.sct.quanda.registration;

import com.sct.quanda.model.UserAccount;
import com.sct.quanda.service.UserAccountService;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationListener;
import org.springframework.mail.SimpleMailMessage;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.stereotype.Component;

/**
 *
 * @author stavares
 */
@Component
public class RegistrationListener implements ApplicationListener<OnRegistrationCompleteEvent>{

  @Autowired
  UserAccountService userAccountService;

  @Autowired
  JavaMailSender javaMailSender;

  private static final Logger LOG = Logger.getLogger(RegistrationListener.class.getName());

  @Override
  public void onApplicationEvent(OnRegistrationCompleteEvent event) {
    LOG.info("Recived event OnRegistrationCompleteEvent");
    UserAccount userAccount = event.getUserAccount();

    String verificationUrl =
            event.getVerificationUrl() + "?token=" +
            event.getVerificationtoken().getToken();

    LOG.info("Create Verivication URL: " + verificationUrl);


    SimpleMailMessage simpleMailMessage = new SimpleMailMessage();
    simpleMailMessage.setTo("urquanda@gmail.com");
    simpleMailMessage.setFrom("urquanda@gmail.com");
    simpleMailMessage.setSubject("User registration.");
    simpleMailMessage.setText("Restered user " +
            event.getUserAccount().getLastname() + ", " +
            event.getUserAccount().getLastname() + ": " +
            event.getUserAccount().getEmail() + "\n\r\n\r" +
            verificationUrl
            );

    javaMailSender.send(simpleMailMessage);
  }
}
