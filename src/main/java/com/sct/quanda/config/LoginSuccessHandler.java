package com.sct.quanda.config;

import java.io.IOException;
import java.util.logging.Logger;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.env.Environment;
import org.springframework.security.core.Authentication;
import org.springframework.security.web.authentication.SavedRequestAwareAuthenticationSuccessHandler;

/**
 *
 * @author stavares
 */
public class LoginSuccessHandler extends SavedRequestAwareAuthenticationSuccessHandler {

  private static final Logger LOG
          = Logger.getLogger(LoginSuccessHandler.class.getName());

  @Autowired
  private Environment env;

  public LoginSuccessHandler(String defaultTargetUrl) {
    super();
    setDefaultTargetUrl(defaultTargetUrl);
    setAlwaysUseDefaultTargetUrl(false);
  }

  @Override
  public void onAuthenticationSuccess(
          HttpServletRequest request,
          HttpServletResponse response,
          Authentication authentication) throws ServletException, IOException {

    LOG.info("LoginSuccessHandler: onAuthenticationSuccess");
    request.getSession().setMaxInactiveInterval(60 *
            Integer.parseInt(env.getProperty("session.timeout")));
    super.onAuthenticationSuccess(request, response, authentication);
  }
}
