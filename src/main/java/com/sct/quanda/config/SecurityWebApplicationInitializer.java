package com.sct.quanda.config;

import org.springframework.security.web.context.AbstractSecurityWebApplicationInitializer;

/**
 *
 * Will automatically register the springSecurityFilterChain Filter for
 * every URL in your application.
 *
 * @author stavares
 */
public class SecurityWebApplicationInitializer
        extends AbstractSecurityWebApplicationInitializer {
}
