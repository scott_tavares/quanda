package com.sct.quanda.config;

import com.sct.quanda.model.Privilege;
import com.sct.quanda.model.Role;
import com.sct.quanda.model.UserAccount;
import com.sct.quanda.service.UserAccountService;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import javax.servlet.http.HttpServletRequest;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 *
 * @author stavares
 */
@Transactional
@Service("userDetailsService")
public class LoginUserDetailsService implements UserDetailsService {

  private static final Logger LOG = Logger.getLogger(LoginUserDetailsService.class.getName());

  @Autowired
  private UserAccountService userAccountService;

//  TODO: Implement LoginAttemptService
//  @Autowired
//  private LoginAttemptService loginAttemptService;
  @Autowired
  private HttpServletRequest request;

  @Autowired
  PasswordEncoder passwordEncoder;

  public LoginUserDetailsService() {
    super();
  }

  @Override
  public UserDetails loadUserByUsername(final String email) throws UsernameNotFoundException {
    boolean enabled = false;
    boolean accountNonExpired = true;
    boolean credentialsNonExpired = true;
    boolean accountNonLocked = true;

//    final String ip = getClientIP();
//    if (loginAttemptService.isBlocked(ip)) {
//      throw new RuntimeException("blocked");
//    }
    try {
      final UserAccount user = userAccountService.findByEmail(email);
      return new org.springframework.security.core.userdetails.User(
          user.getEmail(),
          user.getPassword(),
          user.isEnabled(),
          accountNonExpired,
          credentialsNonExpired,
          accountNonLocked,
          getAuthorities(user.getRoleCollection()));

    } catch (final UsernameNotFoundException e) {
      throw new RuntimeException(e);
    }
  }

  // TODO: Implement getClientIP()
  private String getClientIP() {
    final String xfHeader = request.getHeader("X-Forwarded-For");
    if (xfHeader == null) {
      return request.getRemoteAddr();
    }
    return xfHeader.split(",")[0];
  }

  private Collection<? extends GrantedAuthority>
      getAuthorities(final Collection<Role> roles) {

//    //TODO: Add Roles as Authorities
//    List<GrantedAuthority> authorities = new ArrayList<>();
//    for (String role : roles) {
//        authorities.add(new SimpleGrantedAuthority(role));
//    }
//    return authorities;
    return getGrantedAuthorities(getPrivileges(roles));
  }

  private List<String> getPrivileges(final Collection<Role> roles) {
    final List<String> privileges = new ArrayList<>();
    final List<Privilege> collection = new ArrayList<>();

    for (final Role role : roles) {
      collection.addAll(role.getPrivilegeCollection());
    }

    for (final Privilege item : collection) {
      privileges.add(item.getName());
    }

    return privileges;
  }

  private List<GrantedAuthority> getGrantedAuthorities(final List<String> privileges) {
    final List<GrantedAuthority> authorities = new ArrayList<>();
    for (final String privilege : privileges) {
      authorities.add(new SimpleGrantedAuthority(privilege));
    }
    return authorities;
  }
}
