package com.sct.quanda.config;

import java.util.Locale;
import java.util.Properties;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;
import org.springframework.context.support.ReloadableResourceBundleMessageSource;
import org.springframework.core.env.Environment;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.mail.javamail.JavaMailSenderImpl;
import org.springframework.web.servlet.LocaleResolver;
import org.springframework.web.servlet.ViewResolver;
import org.springframework.web.servlet.config.annotation.DefaultServletHandlerConfigurer;
import org.springframework.web.servlet.config.annotation.EnableWebMvc;
import org.springframework.web.servlet.config.annotation.InterceptorRegistry;
import org.springframework.web.servlet.config.annotation.ResourceHandlerRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurerAdapter;
import org.springframework.web.servlet.i18n.CookieLocaleResolver;
import org.springframework.web.servlet.i18n.LocaleChangeInterceptor;
import org.springframework.web.servlet.view.InternalResourceViewResolver;
import org.springframework.web.servlet.view.JstlView;

/**
 *
 * @author stavares
 */
@EnableWebMvc
@Configuration
@ComponentScan(basePackages = {
  "com.sct.quanda.config",
  "com.sct.quanda.domain",
  "com.sct.quanda.dao",
  "com.sct.quanda.service",
  "com.sct.quanda.web.error",
  "com.sct.quanda.registration"})
@PropertySource({
  "/WEB-INF/props/quanda.properties",
  "/WEB-INF/props/jdbc.properties",
  "/WEB-INF/props/email.properties"})
public class WebXmlConfig extends WebMvcConfigurerAdapter {

  @Autowired
  private Environment env;

  @Override
  public void configureDefaultServletHandling(DefaultServletHandlerConfigurer configurer) {
    configurer.enable();
  }

  @Bean
  public ViewResolver viewResolver() {
    InternalResourceViewResolver viewResolver = new InternalResourceViewResolver();
    viewResolver.setViewClass(JstlView.class);
    viewResolver.setPrefix("/WEB-INF/jsp/");
    viewResolver.setSuffix(".jsp");

    return viewResolver;
  }

  @Override
  public void addInterceptors(InterceptorRegistry registry) {
    LocaleChangeInterceptor localeChangeInterceptor = new LocaleChangeInterceptor();
    localeChangeInterceptor.setParamName("lang");
    registry.addInterceptor(localeChangeInterceptor);
  }

  @Bean
  public LocaleResolver localeResolver() {
    CookieLocaleResolver cookieLocaleResolver = new CookieLocaleResolver();
    cookieLocaleResolver.setDefaultLocale(Locale.ENGLISH);
    cookieLocaleResolver.setCookieName("quanda-locale-cookie");
    cookieLocaleResolver.setCookieMaxAge(Integer.MAX_VALUE);
    return cookieLocaleResolver;
  }

  @Bean
  public MessageSource messageSource() {
    ReloadableResourceBundleMessageSource messageSource
            = new ReloadableResourceBundleMessageSource();
    messageSource.setBasename("classpath:messages");
    messageSource.setUseCodeAsDefaultMessage(true);
    messageSource.setDefaultEncoding("UTF-8");
    messageSource.setCacheSeconds(0);
    return messageSource;
  }

  @Override
  public void addResourceHandlers(ResourceHandlerRegistry registry) {
    registry.addResourceHandler("/resources/**")
            .addResourceLocations("/resources/");
    registry.addResourceHandler("/resources/js/**")
            .addResourceLocations("/resources/js/");
    registry.addResourceHandler("/resources/js/bootstrap/**")
            .addResourceLocations("/resources/js/bootstrap/");
  }

  @Bean
  public JavaMailSender javaMailSenderImpl(){
    Properties javaMailProperties = new Properties();
    JavaMailSenderImpl javaMailSender = new JavaMailSenderImpl();
    javaMailProperties.put("mail.transport.protocol",
            env.getProperty("spring.mail.properties.mail.transport.protocol"));
    javaMailProperties.put("mail.smtp.auth",
            env.getProperty("spring.mail.properties.mail.smtps.auth"));
    javaMailProperties.put("mail.smtp.starttls.enable",
            env.getProperty("spring.mail.properties.mail.smtps.starttls.enable"));
    javaMailProperties.put("mail.debug",
            env.getProperty("spring.mail.properties.mail.debug"));

    javaMailSender.setJavaMailProperties(javaMailProperties);
    javaMailSender.setHost(env.getProperty("spring.mail.host"));
    javaMailSender.setPort(Integer.parseInt(env.getProperty("spring.mail.port")));
    javaMailSender.setProtocol(env.getProperty("spring.mail.protocol"));
    javaMailSender.setUsername(env.getProperty("spring.mail.username"));
    javaMailSender.setPassword(env.getProperty("spring.mail.password"));

    return javaMailSender;
  }
}
