package com.sct.quanda.config;

import java.util.Properties;
import java.util.logging.Logger;
import javax.naming.NamingException;
import javax.sql.DataSource;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.env.Environment;
import org.springframework.jndi.JndiTemplate;
import org.springframework.orm.jpa.JpaVendorAdapter;
import org.springframework.orm.jpa.LocalContainerEntityManagerFactoryBean;
import org.springframework.orm.jpa.vendor.EclipseLinkJpaVendorAdapter;
import org.springframework.transaction.PlatformTransactionManager;
import org.springframework.transaction.annotation.EnableTransactionManagement;
import org.springframework.transaction.jta.JtaTransactionManager;

/**
 *
 * @author stavares
 */
@Configuration
@ComponentScan({"com.sct.quanda.web"})
//@EnableJpaRepositories("com.sct.quanda.dao")
@EnableTransactionManagement
public class PersistenceJNDIConfig {

  private static final Logger LOG
          = Logger.getLogger(PersistenceJNDIConfig.class.getName());

  @Autowired
  private Environment env;

  @Bean
  public LocalContainerEntityManagerFactoryBean entityManagerFactory()
          throws NamingException {

    LocalContainerEntityManagerFactoryBean em
            = new LocalContainerEntityManagerFactoryBean();

    JpaVendorAdapter vendorAdapter = new EclipseLinkJpaVendorAdapter();
    em.setJpaVendorAdapter(vendorAdapter);
//    em.setJpaProperties(jpaProperties());
    em.setDataSource(dataSource());

    return em;
  }

  @Bean
  public DataSource dataSource() throws NamingException {
    DataSource ds =
            (DataSource) new JndiTemplate().lookup(env.getProperty("jndi.url"));

    return ds;
  }

  @Bean
   public PlatformTransactionManager transactionManager() throws NamingException{
      JtaTransactionManager transactionManager = new JtaTransactionManager();
      return transactionManager;
   }

  private Properties jpaProperties() {
    Properties properties = new Properties();
//      properties.setProperty("hibernate.hbm2ddl.auto", "create-drop");
//      properties.setProperty("hibernate.dialect", "org.hibernate.dialect.MySQL5Dialect");
      return properties;
  }
}
