package com.sct.quanda.config;

import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;

/**
 *
 * @author stavares
 */
@Configuration
@ComponentScan(basePackages = {
  "com.sct.quanda.domain",
  "com.sct.quanda.domain.acl",
  "com.sct.quanda.dao",
  "com.sct.quanda.service"})
public class RootContextConfig {
}
