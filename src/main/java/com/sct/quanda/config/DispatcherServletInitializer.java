package com.sct.quanda.config;

import java.util.HashSet;
import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.ServletRegistration;
import javax.servlet.SessionTrackingMode;
import org.springframework.web.WebApplicationInitializer;
import org.springframework.web.context.support.AnnotationConfigWebApplicationContext;
import org.springframework.web.servlet.DispatcherServlet;

/**
 *
 * @author stavares
 */
public class DispatcherServletInitializer implements WebApplicationInitializer {

  @Override
  public void onStartup(ServletContext servletContext) throws ServletException {

    AnnotationConfigWebApplicationContext webContext
            = new AnnotationConfigWebApplicationContext();

    HashSet<SessionTrackingMode> sessionTrackingModes = new HashSet<>();
    sessionTrackingModes.add(SessionTrackingMode.COOKIE);
    servletContext.setSessionTrackingModes(sessionTrackingModes);
    webContext.register(WebXmlConfig.class);
    webContext.setServletContext(servletContext);

    ServletRegistration.Dynamic servlet
            = servletContext
                    .addServlet("dispatcher", new DispatcherServlet(webContext));

    servlet.setLoadOnStartup(1);
    servlet.addMapping("/");
  }
}
