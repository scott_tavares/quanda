package com.sct.quanda.config;

import javax.sql.DataSource;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.AuthenticationProvider;
import org.springframework.security.authentication.dao.DaoAuthenticationProvider;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.security.web.authentication.AuthenticationFailureHandler;
import org.springframework.security.web.csrf.CsrfTokenRepository;
import org.springframework.security.web.csrf.HttpSessionCsrfTokenRepository;

@Configuration
@EnableWebSecurity
@ComponentScan(basePackages = {"com.sct.quanda.security"})
public class LoginSecurityConfig extends WebSecurityConfigurerAdapter {

  private static final Logger LOG
          = Logger.getLogger(LoginSecurityConfig.class.getName());

  @Autowired
  private UserDetailsService userDetailsService;

  @Autowired
  private DataSource dataSource;

  @Autowired
  PasswordEncoder passwordEncoder;

  @Override
  public void configure(HttpSecurity http) throws Exception {
    http.authorizeRequests()
            .antMatchers("/").permitAll()
            .antMatchers("/reg/**").permitAll()
            .antMatchers("/login*").permitAll()
            .antMatchers("/error/**").permitAll()
            .antMatchers("/emailConfirm*").permitAll()
            .antMatchers("/resources/**").permitAll()
            .antMatchers("/resources/js/**").permitAll()
            .antMatchers("/resources/js/bootstrap/**").permitAll()
            .antMatchers("/resources/css/bootstrap/**").permitAll()
            .antMatchers("/**").authenticated()
            .and()
            .rememberMe()
            .and()
            .headers().contentTypeOptions().and()
            .and()
            .headers().cacheControl().and()
            .and()
            .headers().xssProtection().and()
            .and()
            .formLogin()
            .successHandler(loginSuccessHandlerBean("/home"))
            .loginPage("/login")
            .failureHandler(loginFailureHandlerBean())
            .usernameParameter("username")
            .passwordParameter("password")
            .and()
            .logout()
            .logoutSuccessUrl("/login?logout")
            .invalidateHttpSession(true)
            .deleteCookies("JSESSIONID")
            .and()
            .csrf().csrfTokenRepository(csrfTokenRepository())
            .and()
            .exceptionHandling().accessDeniedPage("/accessdenied");
  }

  @Bean
  @Override
  public AuthenticationManager authenticationManagerBean() throws Exception {
    return super.authenticationManagerBean();
  }

  @Override
  @Autowired
  protected void configure(final AuthenticationManagerBuilder authenticationMgr)
          throws Exception {
    authenticationMgr.authenticationProvider(authenticationProvider());
  }

  @Bean
  public PasswordEncoder passwordEncoder() {
    return new BCryptPasswordEncoder(17);
  }

  @Bean
  public LoginSuccessHandler loginSuccessHandlerBean(String defaultTargetUrl) {
    final LoginSuccessHandler handeler = new LoginSuccessHandler(defaultTargetUrl);
    handeler.setAlwaysUseDefaultTargetUrl(false);
    return handeler;
  }

  @Bean
  public AuthenticationFailureHandler loginFailureHandlerBean() {
    final LoginFailureHandler handeler = new LoginFailureHandler();
    return handeler;
  }

  private CsrfTokenRepository csrfTokenRepository() {
    LOG.info("CsrfTokenRepository - Creation....");
    HttpSessionCsrfTokenRepository repository =
            new HttpSessionCsrfTokenRepository();
    repository.setSessionAttributeName("_csrf");
    return repository;
  }

  private AuthenticationProvider authenticationProvider() {
    final DaoAuthenticationProvider daoAuthenticationProvider
            = new DaoAuthenticationProvider();
    daoAuthenticationProvider.setUserDetailsService(userDetailsService);
    daoAuthenticationProvider.setPasswordEncoder(passwordEncoder);
    return daoAuthenticationProvider;
  }
}
