package com.sct.quanda.dao;

import com.sct.quanda.model.UserAccount;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import org.apache.log4j.Logger;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

/**
 *
 * @author stavares
 */
@Repository
public class UserAccountDaoImpl implements UserAccountDao {

  private static final Logger LOG = Logger.getLogger(UserAccountDaoImpl.class.getName());

  @PersistenceContext
  private EntityManager em;

  @Override
  @Transactional
  public UserAccount findByEmail(String email) {
    Query q = em.createQuery("SELECT u FROM UserAccount u WHERE u.email = :email");
    q.setParameter("email", email);
    em.flush();
    UserAccount userAccount = (UserAccount) q.getSingleResult();
    em.refresh(userAccount);
    return userAccount;
  }

  @Override
  @Transactional
  public UserAccount create(UserAccount userAccount) {
    LOG.debug("In UserAccountDaoImpl::create()");
    em.persist(userAccount);
    em.flush();
    LOG.debug("Returning from UserAccountDaoImpl::create():: id is: " + userAccount.getId());
    return userAccount;
  }

  @Override
  @Transactional
  public void delete(UserAccount userAccount) {
    userAccount = em.merge(userAccount);
    em.remove(userAccount);
    em.flush();
  }

  @Override
  @Transactional
  public UserAccount save(UserAccount userAccount) {
    LOG.debug("UserAccountDaoImpl::save::userAccount.isEnabled: " + userAccount.isEnabled());
    em.merge(userAccount);
    em.flush();
    return userAccount;
  }
}
