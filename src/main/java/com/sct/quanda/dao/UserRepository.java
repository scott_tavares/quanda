package com.sct.quanda.dao;

import com.sct.quanda.model.UserAccount;
import org.springframework.stereotype.Repository;

@Repository
public interface UserRepository /* extends JpaRepository<User, Long>*/ {
    UserAccount findByEmail(String email);

//    @Override
    void delete(UserAccount user);
}
