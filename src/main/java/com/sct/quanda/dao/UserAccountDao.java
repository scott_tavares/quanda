package com.sct.quanda.dao;

import com.sct.quanda.model.UserAccount;

/**
 *
 * @author stavares
 */
public interface UserAccountDao {
  UserAccount findByEmail(String email);
  public UserAccount create(UserAccount user);
  public void delete(UserAccount userAccount);
  public UserAccount save(UserAccount userAccount);
}
