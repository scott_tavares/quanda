package com.sct.quanda.dao;

import com.sct.quanda.model.Verificationtoken;

/**
 *
 * @author stavares
 */
public interface VerificationtokenDao {
  public Verificationtoken save(Verificationtoken myToken);
  public void create(Verificationtoken myToken);
  public Verificationtoken findByToken(String token);
}
