package com.sct.quanda.dao;

import com.sct.quanda.model.Quanda;
import java.util.List;

/**
 *
 * @author stavares
 */
public interface QuandaDao {
  List<Quanda> findAll();
}
