package com.sct.quanda.dao;

import com.sct.quanda.model.Quanda;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import org.springframework.stereotype.Repository;

/**
 *
 * @author stavares
 */
@Repository
public class QuandaDaoImpl implements QuandaDao {

  @PersistenceContext
  private EntityManager em;

  @Override
  public List<Quanda> findAll() {
    return em.createQuery("SELECT q FROM Quanda q").getResultList();
  }
}
