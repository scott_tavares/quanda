package com.sct.quanda.dao;

import com.sct.quanda.model.Verificationtoken;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

/**
 *
 * @author stavares
 */
@Repository
public class VerificationtokenDaoImpl implements VerificationtokenDao {

  @PersistenceContext
  private EntityManager em;

  @Override
  @Transactional
  public Verificationtoken save(Verificationtoken verificationtoken) {
    em.merge(verificationtoken);
    em.flush();
    return verificationtoken;
  }

  @Override
  public Verificationtoken findByToken(String token) {
    Query q = em.createQuery("SELECT v FROM Verificationtoken v WHERE v.token = :token");
    q.setParameter("token", token);
    return (Verificationtoken)q.getSingleResult();
  }

  @Override
  @Transactional
  public void create(Verificationtoken myToken) {
    em.persist(myToken);
  }
}
