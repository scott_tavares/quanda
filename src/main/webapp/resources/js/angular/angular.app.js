(function () {
  var app = angular.module('gemStore', []);

  app.controller('StoreController', function () {
    this.products = gems;
  });

  app.controller('PanelController', function () {
    this.tab = 1;

    console.log('PanelController');

    this.selectTab = function (setTab) {
      this.tab = setTab;
    };

    this.isSelected = function (checkTab) {
      return this.tab === checkTab;
    };

  });

  app.controller("ReviewController", function () {
    this.review = {};
    console.log('ReviewController');

    this.addReview = function (product) {
      this.review.createdOn = Date.now();
      product.reviews.push(this.review);
      this.review = {};
      console.log('added the product');
    };

  });

  var gems = [
    {
      name: 'Diamond',
      price: 110.00,
      description: 'Some description of this gem.',
      canPurchase: true,
      soldOut: false,
      images: [
        {
          full: 'gem-01-full.jpg',
          thumb: 'gem-01-thumb.gif'
        },
        {
          full: 'dodecahedron-02-full.jpg',
          thumb: 'dodecahedron-02-thumb.jpg'
        }
      ],
      reviews: [
        {
          stars: 5,
          body: "Great product!",
          author: "stavares@thecityside.com"
        },
        {
          stars: 1,
          body: "Product Sucks!",
          author: "jj@thecityside.com"
        }
      ]
    },
    {
      name: 'Ruby',
      price: 510.00,
      description: 'Some description of this gem.',
      canPurchase: false,
      soldOut: false,
      images: [
        {
          full: 'gem-01-full.jpg',
          thumb: 'gem-02-thumb.gif'
        },
        {
          full: 'dodecahedron-02-full.jpg',
          thumb: 'dodecahedron-02-thumb.jpg'
        }
      ],
      reviews: [
        {
          stars: 5,
          body: "Great product!",
          author: "stavares@thecityside.com"
        },
        {
          stars: 1,
          body: "Product Sucks!",
          author: "jj@thecityside.com"
        }
      ]
    }
  ];
})();


