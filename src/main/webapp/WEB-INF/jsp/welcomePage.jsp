<!DOCTYPE html>
<%@ page contentType="text/html" pageEncoding="UTF-8"%>
<%@ page session="true"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://www.springframework.org/security/tags" prefix ="security" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring" %>
<html>  
  <body>
    <fmt:setLocale value="en_US" />
    <h2>WELCOME TO QUANDA</h2><br>
    <a href="?lang=en">English</a> | <a href="?lang=es_ES">Spanish</a><br><br>
    <a href="${pageContext.request.contextPath}/login">Login to Quanda</a>
    <p>    
    <a href="${pageContext.request.contextPath}/reg/new">Register</a>
  </body>
</html>
