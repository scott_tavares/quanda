<%-- 
    Document   : registationPage
    Created on : Nov 8, 2017, 6:19:39 AM
    Author     : stavares
--%>

<%-- <spring:message code="message.badCredentials" /> --%>
<!DOCTYPE html>
<%@ page session="true"%>
<%@ page contentType="text/html" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring" %>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ taglib uri="http://www.springframework.org/security/tags" prefix ="security" %>
<html>
  <head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <title><spring:message code="static.registrationpage.title" /></title>
  </head>
  <body  onload='document.f.firstName.focus();'>
    <h1><spring:message code="static.registrationpage.welcome" /></h1>
    <h3>Session Scope (key==values)</h3>    
    <div>
      <form:form commandName="newUser" name='f' method="POST"
                 action="${pageContext.request.contextPath}/reg?fdAction=create">
        <table>
          <tr>
            <td>First Name</td>
            <td>
              <form:input path="firstName" />
              <font style="color: #C11B17;">
                <div class="form-errors">
                  <form:errors path="firstName"/>
                  <span id="firstNameErrors"></span>
                </div>
              </font>
            </td>
          </tr>
          <tr>
            <td>Last Name</td>
            <td><form:input path="lastName" />
              <font style="color: #C11B17;">
              <form:errors path="lastName"/>
              </font></td>
          </tr>
          <tr>
            <td>Email</td>
            <td><form:input path="email" />
              <font style="color: #C11B17;">
              <form:errors path="email"/>
              </font></td>
          </tr>
          <tr>
            <td>Password</td>
            <td><form:password  path="password" />
              <font style="color: #C11B17;">
              <form:errors path="password"/>              
              </font></td>
          </tr>
          <tr>
            <td>Password Confirm</td>
            <td><form:password path="passwordMatcher" />
              <font style="color: #C11B17;">              
              <form:errors path="passwordMatcher"/>                           
              </font>                       
            </td>              
          </tr>
          <tr>
            <td>
              <table style="padding-left: 100px;">
                <tr align="center">
                  <td class="td">
                    <input type="submit" value="Save" />
                    <a href="${pageContext.request.contextPath}/home"
                       style="color: green"><b>Go Back</b></a>
                  </td>
                </tr>
              </table>
            </td>
          </tr>
        </table>
      </form:form>
    </div>
    <script src="${pageContext.request.contextPath}/resources/js/jquery/jquery-3.2.1.min.js"></script>
    <script type="text/javascript">
    <!-- var serverContext = [[@{/}]]; -->
    
    var serverContext = window.location.pathname
                .substring(0, window.location.pathname.indexOf("/", 2));

    function register() {
      $(".alert").html("").hide();
      var formData = $('form').serialize();
      
      $.post(serverContext + "/user/registration", formData, function(data){
        if (data.message == "success"){
          window.location.href = serverContext + "/successRegister.html";
        }
      }).fail(function(data) {
        if (data.responseJSON.error.indexOf("MailError") > - 1){
          window.location.href = serverContext + "/emailError.html";
        } else if (data.responseJSON.error.indexOf("InternalError") > - 1) {
          window.location.href = serverContext +
              "/login.html?message=" + data.responseJSON.message;
        } else if (data.responseJSON.error == "UserAlreadyExist"){
          $("#emailError").show().html(data.responseJSON.message);
        } else {
          var errors = $.parseJSON(data.responseJSON.message);
          $.each(errors, function(index, item){
            $("#" + item.field + "Error").show().html(item.defaultMessage);
          });
          errors = $.parseJSON(data.responseJSON.error);
          $.each(errors, function(index, item){
            $("#globalError").show().append(item.defaultMessage + "<br>");
          });
      }
    }
    </script>
  </body>
</html>
