<%-- 
    Document   : badUserPage
    Created on : Nov 22, 2017, 8:21:57 AM
    Author     : stavares
--%>
<!DOCTYPE html>
<%@ page contentType="text/html" pageEncoding="UTF-8"%>
<%@ page session="true"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<html>
  <head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <title>Bad User Account</title>
  </head>
  <body>
    <c:if test="${not empty message}">
      <div style="font-family: 'arial'; font-size: 16px; font-weight: bolder; color: red;">
        <c:out value="${message}" />
      </div>
    </c:if>
    <c:if test="${expired}">
      <button onclick="resendToken()">Resend Token</button>
      <script type="text/javascript" charset="UTF-8" 
        src="${pageContext.request.contextPath}/resources/js/jquery/jquery-3.2.1.min.js" />
      <script type="text/javascript">
        var serverContext = window.location.pathname
                .substring(0, window.location.pathname.indexOf("/", 2));

        function resendToken() {
          $.get(serverContext + "/reg/resendRegistrationToken?token=${token}",
                  function (data) {
                    window.location.href =
                            serverContext + "/login.html?message=" + data.message;
                  })
                  .fail(function (data) {
                    if (data.responseJSON.error.indexOf("MailError") > -1) {
                      window.location.href = serverContext + "emailError.html";
                    } else {
                      window.location.href =
                              serverContext + "/login?message=" + data.responseJSON.message;
                    }
                  });
        }
      </script>
    </c:if>
  </body>
</html>
