<!DOCTYPE html>
<%@ page contentType="text/html" pageEncoding="UTF-8"%>
<%@ page session="true"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://www.springframework.org/security/tags" prefix ="security" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring" %>
<html>
  <head>    
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <title>QUANDA</title>
  </head>
  <body>    
    <h1 style="font-family: 'arial'; font-size: 16px; font-weight: bolder; color: red;">
      <spring:message code="message.unauth" />
    </h1>
    <br />
    <a href="${pageContext.request.contextPath}/home">Back</a>
  </body>
</html>

