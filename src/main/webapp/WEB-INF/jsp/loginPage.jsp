<%-- 
    Document   : registationPage
    Created on : Nov 8, 2017, 6:19:39 AM
    Author     : stavares
--%>
<!DOCTYPE html>
<%@ page contentType="text/html" pageEncoding="UTF-8"%>
<%@ page session="true"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://www.springframework.org/security/tags" prefix ="security" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring" %>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<html>  
  <head>
    <security:csrfMetaTags />
    <title><spring:message code="static.loginpage.title" /></title>
  </head>
  <body onload='document.f.username.focus();'>    
    <h3><spring:message code="static.loginpage.welcome" /></h3>
    <c:if test="${not empty sessionScope.SPRING_SECURITY_LAST_EXCEPTION}">
      <div style="font-family: 'arial'; font-size: 16px; font-weight: bolder; color: red;">
        <spring:message code="${sessionScope.SPRING_SECURITY_LAST_EXCEPTION}" />      
      </div>
    </c:if>   
    <c:if test="${not empty message}">
      <div style="font-family: 'arial'; font-size: 16px; font-weight: bolder; color: red;">
        <spring:message code="message.logoutSucc" />
      </div>
    </c:if>
    <c:if test="${not empty registration}">
      <div style="font-family: 'arial'; font-size: 16px; font-weight: bolder; color: red;">
        <spring:message code="message.registration.successeeded" />
      </div>
    </c:if>
    <br>
    <form:form name='f' action='/quanda/login' onsubmit="return validateForm()" method='POST'>
      <table>
        <tr>
          <td>User:</td>
          <td>
            <input type='text' name='username'>
          </td>
        </tr>
        <tr>
          <td>Password:</td>
          <td><input type='password' name='password'/></td>
        </tr>
        <tr>
          <td colspan='2'>
            <input name="submit" type="submit" value="Login"/>
            <a href="${pageContext.request.contextPath}/">
              <button type="button">Cancel</button>
            </a>
          </td>
        </tr>          
      </table>
      <security:csrfInput/>      
    </form:form>
    <%--
    <script src="https://code.jquery.com/jquery.js"></script>
    <script type="text/javascript" charset="UTF-8" src="${pageContext.request.contextPath}/resources/js/bootstrap/bootstrap.min.js"></script>
    --%>
    <script type="text/javascript" charset="UTF-8" src="${pageContext.request.contextPath}/resources/js/app.js"></script>    
  </body>
</html>
