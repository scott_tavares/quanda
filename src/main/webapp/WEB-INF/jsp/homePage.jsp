<%-- 
    Document   : homePage
    Created on : Sep 28, 2017, 6:29:29 PM
    Author     : stavares
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@taglib uri="http://www.springframework.org/security/tags" prefix ="security" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring" %>
<!DOCTYPE html>
<html>
  <head>    
    <security:csrfMetaTags />
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <title>Home Page</title>
  </head>
  <body>
    <h3>Quanda Home Page</h3>
    <h3>Nice to see you back: <c:out value="${user.email}" /></h3>
    <form id="logoutForm" method="POST" action="${pageContext.request.contextPath}/logout">
      <security:csrfInput/>
    </form>
    <a href="${pageContext.request.contextPath}/top/page">Next</a>        
    <c:url value="/logout" var="logoutUrl" />    
    <form id="logout" action="${logoutUrl}" method="post" >
      <security:csrfInput/>
    </form>     
    <c:if test="${pageContext.request.userPrincipal.name != null}">
      <a href="javascript:document.getElementById('logout').submit()">Logout</a>
    </c:if>    
  </body>
</html>
