<%-- 
    Document   : page
    Created on : Sep 14, 2017, 7:38:04 PM
    Author     : stavares
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<!DOCTYPE html>
<html>
  <head>    
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <title>QUANDA</title>
  </head>
  <body>    
    <h1><c:out value="${msg}"/></h1>
    <table align="left" style="padding-left: 10px;">
      <c:forEach items="${quandaList}" var="quanda">
        <tr>
          <td>
            <h2><c:out value="${quanda.idquanda}" /></h2>
          </td>
          <td>
            <h2><c:out value="${quanda.title}" /></h2>
          </td>          
        </tr>
      </c:forEach>
      <tr>
        <td><a href="${pageContext.request.contextPath}/home">Back</a></td>
        <td></td>
      </tr>
    </table>   
  </body>
</html>
